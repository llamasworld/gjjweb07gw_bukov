package com.getjavajob.bukovg.lbo.common.model;

import javax.persistence.*;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

@MappedSuperclass
public abstract class GenericModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
