package com.getjavajob.bukovg.lbo.common.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

@Entity(name = "Requests")
public class Request extends GenericModel {

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date = new Date();

    @ManyToOne
    @JoinColumn(name = "from_id")
    private Account fromAcc;

    @ManyToOne
    @JoinColumn(name = "to_acc_id")
    private Account toAcc;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private RequestType type;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private RequestStatus status = RequestStatus.WAITING;

    @Transient
    private Group toGroup;

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Getters and setters - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Account getFromAcc() {
        return fromAcc;
    }

    public void setFromAcc(Account fromAcc) {
        this.fromAcc = fromAcc;
    }

    public Account getToAcc() {
        return toAcc;
    }

    public void setToAcc(Account toAcc) {
        this.toAcc = toAcc;
    }

    public Group getToGroup() {
        return toGroup;
    }

    public void setToGroup(Group toGroup) {
        this.toGroup = toGroup;
    }

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Object's methods - - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    @Override
    public String toString() {
        if (toAcc != null) {
            return id + " | " + type + " | " + status +
                    " | \nFROM:" + fromAcc.getfName() + " " + fromAcc.getlName() +
                    " | \nTO:" + toAcc.getfName() + " " + toAcc.getlName();
        }
        if (toGroup != null) {
            return id + " | " + type + " | " + status +
                    " | \nFROM:" + fromAcc.getfName() + " " + fromAcc.getlName() +
                    " | \nTO:" + toGroup.getName();
        }
        return "Invalid request";
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Request)) {
            return false;
        }
        Request obj = (Request) object;
        return obj.getId() == this.id;
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - - - - - Another - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    public enum RequestType {

        FRIEND_REQ, GROUP_REQ;
    }

    public enum RequestStatus {

        ACCEPTED, REJECTED, WAITING;
    }
}
