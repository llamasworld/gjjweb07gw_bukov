package com.getjavajob.bukovg.lbo.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

@Entity(name = "Groups")
public class Group extends GenericModel {

    @OneToOne
    @JoinColumn(name = "owner_id")
    private Account owner;

    @Column(name = "name")
    private String name;

    @Column(name = "enrollment_date")
    @Temporal(TemporalType.DATE)
    private Date enrollmentDate = new Date();

    @Column(name = "description")
    private String description;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "Groups_Accounts",
            joinColumns = @JoinColumn(name = "id_group"),
            inverseJoinColumns = @JoinColumn(name = "id_account"))
    private List<Account> members = new ArrayList<>();

    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "content_type")
    private String contentType;

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Getters and setters - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(Date enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Account> getMembers() {
        return members;
    }

    public void setMembers(List<Account> members) {
        this.members = members;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Object's methods - - - - - - - - - - - - - - - - - - - - - - - - - -
    */
    @Override
    public String toString() {
        return id + " | " + name + " | " + owner.getfName() + " " + owner.getlName() + " | " + enrollmentDate + " | " + description;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Group)) {
            return false;
        }
        Group obj = (Group) object;
        return this.name.equals(obj.getName());
    }

    @Override
    public int hashCode() {
        int hash = 37;
        hash = hash * 17 + name.hashCode();
        return hash;
    }
}
