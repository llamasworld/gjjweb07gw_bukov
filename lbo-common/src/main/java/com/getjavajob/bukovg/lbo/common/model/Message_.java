package com.getjavajob.bukovg.lbo.common.model;

import com.getjavajob.bukovg.lbo.common.model.Message.MessageType;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by Gleb Bukov on 03.08.16.
 */

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Message.class)
public abstract class Message_ extends GenericModel_ {

    public static volatile SingularAttribute<Message, Date> date;
    public static volatile SingularAttribute<Message, Account> toAcc;
    public static volatile SingularAttribute<Message, Account> fromAcc;
    public static volatile SingularAttribute<Message, String> description;
    public static volatile SingularAttribute<Message, MessageType> type;
    public static volatile SingularAttribute<Message, Group> toGroup;

}

