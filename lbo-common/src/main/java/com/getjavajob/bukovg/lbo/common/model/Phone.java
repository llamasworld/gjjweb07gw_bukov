package com.getjavajob.bukovg.lbo.common.model;

import javax.persistence.*;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

@Entity(name = "Phones")
public class Phone extends GenericModel {

    @ManyToOne // TODO почему с @OneToOne тесты проходят?
    @JoinColumn(name = "phone_owner"/*_fk(id)*/, nullable = false)
    private Account owner;

    @Column(name = "number")
    private String number;

    @Column(name = "description")
    private String description;

    /*
    - - - - - - - - - - - - - - - - - - - - - - - - Constructors - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    */
    public Phone() {
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Getters and setters - - - - - - - - - - - - - - - - - - - - - - - -
     */
    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Object's methods - - - - - - - - - - - - - - - - - - - - - - - - - -
    */
    @Override
    public String toString() {
        return this.owner.getfName() + " " + this.owner.getlName() + this.number;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Phone)) {
            return false;
        }
        Phone obj = (Phone) object;
        return this.number.equals(obj.getNumber());
    }

    @Override
    public int hashCode() {
        int hash = 37;
        hash = hash * 17 + number.hashCode();
        return hash;
    }
}
