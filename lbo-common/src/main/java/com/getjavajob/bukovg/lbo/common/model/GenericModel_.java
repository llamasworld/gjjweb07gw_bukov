package com.getjavajob.bukovg.lbo.common.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by Gleb Bukov on 03.08.16.
 */

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GenericModel.class)
public abstract class GenericModel_ {

    public static volatile SingularAttribute<GenericModel, Integer> id;

}

