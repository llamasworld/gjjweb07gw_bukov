package com.getjavajob.bukovg.lbo.common.model;

import com.getjavajob.bukovg.lbo.common.model.Request.RequestStatus;
import com.getjavajob.bukovg.lbo.common.model.Request.RequestType;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by Gleb Bukov on 03.08.16.
 */

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Request.class)
public abstract class Request_ extends GenericModel_ {

    public static volatile SingularAttribute<Request, Date> date;
    public static volatile SingularAttribute<Request, Account> toAcc;
    public static volatile SingularAttribute<Request, Account> fromAcc;
    public static volatile SingularAttribute<Request, RequestType> type;
    public static volatile SingularAttribute<Request, RequestStatus> status;

}

