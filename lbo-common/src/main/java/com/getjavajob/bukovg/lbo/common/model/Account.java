package com.getjavajob.bukovg.lbo.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.bukovg.lbo.common.type.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

@Entity(name = "Accounts")
public class Account extends GenericModel {

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "first_name", nullable = false)
    private String fName;

    @Column(name = "last_name", nullable = false)
    private String lName;

    @Column(name = "pron_name")
    private String pName;

    @Column(name = "skype")
    private String skype;

    @Column(name = "icq")
    private Integer icq = new Integer(0);

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.user;

    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "content_type")
    private String contentType;

    @JsonIgnore
    @OneToMany(
            mappedBy = "owner",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Phone> phones = new ArrayList<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "Friendship",
            joinColumns = @JoinColumn(name = "id_acc_one", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_acc_two", referencedColumnName = "id"))
    private List<Account> friends = new ArrayList<>();

    @Column(name = "enrollment_date")
    @Temporal(TemporalType.DATE)
    private Date enrollmentDate = new Date();

    @JsonIgnore
    @ManyToMany(mappedBy = "members")
    private List<Group> groups;

    /*
    - - - - - - - - - - - - - - - - - - - - - - - - Constructors - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    public Account() {
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Getters and setters - - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public int getIcq() {
        return icq;
    }

    public void setIcq(Integer icq) {
        this.icq = icq;
    }

    public Date getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(Date enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public List<Account> getFriends() {
        return friends;
    }

    public void setFriends(List<Account> friends) {
        this.friends = friends;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Object's methods - - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    @Override
    public String toString() {
        return id + " | " + email + " | " + fName + " | " + lName + " | " + pName + " | " + enrollmentDate + " | " + role + " | " + skype + " | " + icq + " | " + photo;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Account)) {
            return false;
        }
        Account obj = (Account) object;
        return this.id == obj.getId() && this.email.equals(obj.getEmail());
    }

    @Override
    public int hashCode() {
        int hash = 37;
        hash = hash * 17 + email.hashCode();
        return hash;
    }
}
