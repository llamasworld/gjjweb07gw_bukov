package com.getjavajob.bukovg.lbo.common.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by Gleb Bukov on 03.08.16.
 */

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Phone.class)
public abstract class Phone_ extends GenericModel_ {

    public static volatile SingularAttribute<Phone, Account> owner;
    public static volatile SingularAttribute<Phone, String> number;
    public static volatile SingularAttribute<Phone, String> description;

}

