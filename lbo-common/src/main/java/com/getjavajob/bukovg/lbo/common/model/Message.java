package com.getjavajob.bukovg.lbo.common.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

@Entity
@Table(name = "Messages")
public class Message extends GenericModel {

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date = new Date();

    @ManyToOne
    @JoinColumn(name = "from_id")
    private Account fromAcc;

    @ManyToOne
    @JoinColumn(name = "to_acc_id")
    private Account toAcc;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private MessageType type;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "to_group_id")
    private Group toGroup;

    @Transient
    private byte[] picture;

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Getters and setters - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Account getFromAcc() {
        return fromAcc;
    }

    public void setFromAcc(Account fromAcc) {
        this.fromAcc = fromAcc;
    }

    public Account getToAcc() {
        return toAcc;
    }

    public void setToAcc(Account toAcc) {
        this.toAcc = toAcc;
    }

    public Group getToGroup() {
        return toGroup;
    }

    public void setToGroup(Group toGroup) {
        this.toGroup = toGroup;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String message) {
        this.description = message;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - -  Object's methods - - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    @Override
    public String toString() {
        if (toAcc != null) {
            return id + " | " + date +
                    " | \nFROM:" + fromAcc.getfName() + " " + fromAcc.getlName() +
                    " | \nTO:" + toAcc.getfName() + " " + toAcc.getlName() +
                    " | \n" + type + " | " + description;
        }
        if (toGroup != null) {
            return id + " | " + date +
                    " | \nFROM:" + fromAcc.getfName() + " " + fromAcc.getlName() +
                    " | \nTO:" + toGroup.getName() +
                    " | \n" + type + " | " + description;
        }
        return "Invalid message";
    }

    @Override
    public int hashCode() {
        return description.hashCode() + id;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Message)) {
            return false;
        }
        Message obj = (Message) object;
        return obj.getId() == this.id;
    }

    public enum MessageType {

        GROUP_MSG, PRIVATE_MSG, PUBLIC_MSG,
    }
}
