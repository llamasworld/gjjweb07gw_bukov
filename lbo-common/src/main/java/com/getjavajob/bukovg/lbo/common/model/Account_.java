package com.getjavajob.bukovg.lbo.common.model;

import com.getjavajob.bukovg.lbo.common.type.Role;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by Gleb Bukov on 03.08.16.
 */

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Account.class)
public abstract class Account_ extends GenericModel_ {

    public static volatile SingularAttribute<Account, String> lName;
    public static volatile SingularAttribute<Account, Role> role;
    public static volatile SingularAttribute<Account, Date> enrollmentDate;
    public static volatile SingularAttribute<Account, byte[]> photo;
    public static volatile ListAttribute<Account, Phone> phones;
    public static volatile ListAttribute<Account, Account> friends;
    public static volatile SingularAttribute<Account, String> skype;
    public static volatile SingularAttribute<Account, String> password;
    public static volatile SingularAttribute<Account, String> fName;
    public static volatile SingularAttribute<Account, String> pName;
    public static volatile SingularAttribute<Account, Integer> icq;
    public static volatile SingularAttribute<Account, String> contentType;
    public static volatile SingularAttribute<Account, String> email;

}

