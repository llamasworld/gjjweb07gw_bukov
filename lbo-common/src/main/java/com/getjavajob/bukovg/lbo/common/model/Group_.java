package com.getjavajob.bukovg.lbo.common.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Created by Gleb Bukov on 03.08.16.
 */

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Group.class)
public abstract class Group_ extends GenericModel_ {

    public static volatile SingularAttribute<Group, Account> owner;
    public static volatile SingularAttribute<Group, String> name;
    public static volatile SingularAttribute<Group, Date> date;
    public static volatile SingularAttribute<Group, String> description;
    public static volatile ListAttribute<Group, Account> members;
    public static volatile SingularAttribute<Group, byte[]> photo;
    public static volatile SingularAttribute<Group, String> contentType;
}
