package com.getjavajob.bukovg.lbo.controllers;

import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.common.model.Group;
import com.getjavajob.bukovg.lbo.common.model.Message;
import com.getjavajob.bukovg.lbo.service.dto.AccountService;
import com.getjavajob.bukovg.lbo.service.dto.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Gleb Bukov on 03.08.16.
 */

@Controller
public class GroupController {

    @Autowired
    private GroupService groupService;
    @Autowired
    private AccountService accountService;

    public static final Logger LOG = LoggerFactory.getLogger(ServiceController.class.getName());

    @RequestMapping(value = "/account/groups", method = RequestMethod.GET)
    public ModelAndView groupOverview(@SessionAttribute("id") int ownerId) {
        ModelAndView modelAndView = new ModelAndView("group/groupOverview");
        Account owner = accountService.getAccountById(ownerId);
        modelAndView.addObject("groups", owner.getGroups());
        return modelAndView;
    }

    @RequestMapping(value = "/account/group/join", method = RequestMethod.GET)
    public String joinToGroup(@SessionAttribute("id") int ownerId, @RequestParam("groupId") int groupId) {
        Account owner = accountService.getAccountById(ownerId);
        Group group = groupService.getGroupById(groupId);
        owner.getGroups().add(group);
        group.getMembers().add(owner);
        accountService.updateAccount(owner);
        groupService.updateGroup(group);
        LOG.info(owner.getfName() + " " + owner.getlName() + " join to " + group.getName());
        return "redirect:/account/group?id=" + groupId;
    }

    @RequestMapping(value = "/account/group/leave", method = RequestMethod.GET)
    public String leaveGroup(@SessionAttribute("id") int accountId, @RequestParam("groupId") int groupId) {
        groupService.leaveGroup(groupId, accountId);
        LOG.info("Account with " + accountId + " id leaves group with " + groupId + " id");
        return "redirect:/account/groups";
    }

    @RequestMapping(value = "/account/createGroup", method = RequestMethod.POST)
    public String createGroup(@SessionAttribute("id") int accountId, @RequestParam("groupName") String groupName, @RequestParam("description") String description) {
        Group group = new Group();
        Account owner = accountService.getAccountById(accountId);

        group.setOwner(owner);
        group.setName(groupName);
        group.setDescription(description);
        groupService.createGroup(group);

        group.getMembers().add(owner);
        owner.getGroups().add(group);

        groupService.updateGroup(group);
        accountService.updateAccount(owner);

        return "redirect:/account/groups";
    }

    @RequestMapping(value = "/account/group", method = RequestMethod.GET)
    public ModelAndView showGroupPage(@RequestParam("id") int groupId, @SessionAttribute("id") int ownerId) {
        Group group = groupService.getGroupById(groupId);
        List<Message> messages = groupService.getPublicMessagesForGroup(groupId);
        ModelAndView modelAndView = new ModelAndView("group/groupPage");
        modelAndView.addObject("group", group);
        modelAndView.addObject("messages", messages);
        if (group.getMembers().contains(accountService.getAccountById(ownerId))) {
            modelAndView.addObject("guest", "no");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/account/group/search")
    @ResponseBody
    public List<Group> searchAccounts(@RequestParam("s") String text) {
        List<Group> searchResult = groupService.findGroupByName(text);
        return searchResult;
    }
}
