package com.getjavajob.bukovg.lbo.controllers;

import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.common.model.Message;
import com.getjavajob.bukovg.lbo.service.dto.AccountService;
import com.getjavajob.bukovg.lbo.service.dto.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Gleb Bukov on 18.07.16.
 */

@Controller
public class MessageController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    /**
     * На данной странице отображаются список бесед для все аккаунтов которые есть у
     * у нас в друзьях.
     *
     * @param ownerId
     * @return
     */
    @RequestMapping(value = "/account/messages", method = RequestMethod.GET)
    public ModelAndView showMessagesPage(@SessionAttribute("id") int ownerId) {
        List<Account> friends = accountService.getAllFriends(ownerId);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("messages/privateMessages");
        modelAndView.addObject("friends", friends);
        return modelAndView;
    }

    @RequestMapping(value = "/account/messages/conversation/{opponent}", method = RequestMethod.GET)
    public ModelAndView showConversation(@SessionAttribute("id") int ownerId, @PathVariable("opponent") int opponentId) {
        List<Message> messages = accountService.getMessagesBetweenTwoAccounts(ownerId, opponentId);
        if (messages.size() == 0) {
            // WarningPage(?)
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("messages/privateConversation");
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("opponent", accountService.getAccountById(opponentId));
        modelAndView.addObject("owner", accountService.getAccountById(ownerId));
        return modelAndView;
    }

    @RequestMapping(value = "/account/messages/conversation/send", method = RequestMethod.POST)
    public ModelAndView sendMessage(
            @RequestParam("description") String string,
            @RequestParam("opponentId") int opponentId,
            @SessionAttribute("id") int ownerId) {
        Message message = new Message();
        Account owner = accountService.getAccountById(ownerId);
        Account opponent = accountService.getAccountById(opponentId);
        message.setFromAcc(owner);
        message.setToAcc(opponent);
        message.setDescription(string);
        message.setType(Message.MessageType.PRIVATE_MSG);
        accountService.sendMessageToAccount(message);
        ModelAndView modelAndView = new ModelAndView("redirect:/account/messages/conversation/" + opponentId);
        return modelAndView;
    }

    @RequestMapping(value = "/account/messages/public/send", method = RequestMethod.POST)
    public String sendPublicMessageToAccount(
            @SessionAttribute("id") int ownerId,
            @RequestParam("toAccountId") int toAccountId,
            @RequestParam("publicMessage") String publicMessage) {
        Message message = new Message();
        message.setFromAcc(accountService.getAccountById(ownerId));
        message.setToAcc(accountService.getAccountById(toAccountId));
        message.setDescription(publicMessage);
        message.setType(Message.MessageType.PUBLIC_MSG);
        accountService.sendPublicMessagesToAccount(message);
        return "redirect:/account/user?id=" + toAccountId;
    }

    @RequestMapping(value = "/account/group/messages/public/send", method = RequestMethod.POST)
    public String sendPublicMessageToGroup(
            @SessionAttribute("id") int ownerId,
            @RequestParam("toGroupId") int toGroupId,
            @RequestParam("publicMessage") String publicMessage) {
        Message message = new Message();
        message.setFromAcc(accountService.getAccountById(ownerId));
        message.setToGroup(groupService.getGroupById(toGroupId));
        message.setDescription(publicMessage);
        message.setType(Message.MessageType.PUBLIC_MSG);
        groupService.sendMessageToGroup(message);
        return "redirect:/account/group?id=" + toGroupId;
    }

    @RequestMapping(value = "/account/messages/public/remove", method = RequestMethod.GET)
    public String removePublicMessageFromAccountsWall(@SessionAttribute("id") int ownerId, @RequestParam("messageId") int messageId) {
        accountService.removePublicMessagesFromAccountsWall(messageId);
        return "redirect:/account/user?id=" + ownerId;
    }

    @RequestMapping(value = "/account/group/messages/public/remove", method = RequestMethod.GET)
    public String removePublicMessageFromAccountsWall(
            @SessionAttribute("id") int ownerId,
            @RequestParam("messageId") int messageId,
            @RequestParam("groupId") int groupId) {
        accountService.removePublicMessagesFromAccountsWall(messageId);
        return "redirect:/account/group?id=" + groupId;
    }

    @RequestMapping(value = "/account/loadPublicMessages", method = RequestMethod.GET)
    @ResponseBody
    public List<Message> loadPartOfPublicMessagesForAccount(
            @RequestParam("page") String pageNumber,
            @RequestParam("accountId") String accountId) {
        return accountService.getPublicMessagesForAccountPagination(Integer.valueOf(accountId), Integer.valueOf(pageNumber));
    }
}
