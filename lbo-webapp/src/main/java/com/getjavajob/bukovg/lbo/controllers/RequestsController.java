package com.getjavajob.bukovg.lbo.controllers;

import com.getjavajob.bukovg.lbo.common.model.Request;
import com.getjavajob.bukovg.lbo.service.dto.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Gleb Bukov on 20.07.16.
 */

@Controller
public class RequestsController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/account/requests", method = RequestMethod.GET)
    public ModelAndView showRequests(@SessionAttribute("id") int ownerId) {
        List<Request> requests = accountService.getWaitingRequestsForAnAccount(ownerId);
        ModelAndView modelAndView = new ModelAndView("requests/requestsListPage");
        modelAndView.addObject("requests", requests);
        return modelAndView;
    }

    @RequestMapping(value = "/account/requests/accept", method = RequestMethod.POST)
    public String acceptRequest(@RequestParam("requestId") int requestId) {
        accountService.acceptFriendRequest(requestId);
        return "redirect:/account/requests";
    }

    @RequestMapping(value = "/account/requests/reject", method = RequestMethod.POST)
    public String rejectRequest(@RequestParam("requestId") int requestId) {
        accountService.rejectFriendRequest(requestId);
        return "redirect:/account/requests";
    }
}
