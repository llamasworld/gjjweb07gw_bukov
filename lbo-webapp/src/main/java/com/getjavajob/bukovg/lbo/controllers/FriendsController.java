package com.getjavajob.bukovg.lbo.controllers;

import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.common.model.Request;
import com.getjavajob.bukovg.lbo.service.dto.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by Gleb Bukov on 20.07.16.
 */

@Controller
public class FriendsController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/account/friends", method = RequestMethod.GET)
    public ModelAndView showFriends(@SessionAttribute("id") int ownerId) {
        List<Account> friends = accountService.getAllFriends(ownerId);
        ModelAndView modelAndView = new ModelAndView("friends/friendsListPage");
        modelAndView.addObject("friends", friends);
        return modelAndView;
    }

    @RequestMapping(value = "/account/friends/remove", method = RequestMethod.GET)
    public String removeFriend(@SessionAttribute("id") int ownerId, @RequestParam("friendId") int friendId) {
        if (ownerId != 0 && friendId != 0) {
            accountService.removeFriendship(ownerId, friendId);
        }
        return "redirect:/account/friends";
    }

    @RequestMapping(value = "/account/friends/add", method = RequestMethod.GET)
    public String offerFriendship(@SessionAttribute("id") int ownerId, @RequestParam("friendId") int friendId) {
        Request friendRequest = new Request();
        friendRequest.setFromAcc(accountService.getAccountById(ownerId));
        friendRequest.setToAcc(accountService.getAccountById(friendId));
        friendRequest.setType(Request.RequestType.FRIEND_REQ);
        friendRequest.setStatus(Request.RequestStatus.WAITING);
        accountService.sendFriendRequest(friendRequest);
        return "redirect:/account/user?id=" + friendId;
    }
}
