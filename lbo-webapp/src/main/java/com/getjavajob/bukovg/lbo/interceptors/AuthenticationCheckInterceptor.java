package com.getjavajob.bukovg.lbo.interceptors;

import com.getjavajob.bukovg.lbo.common.model.Account;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Gleb Bukov on 20.07.16.
 */

public class AuthenticationCheckInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession session = request.getSession(true);

        /**
         * Проверяю: вошел ли пользователь в систему, если нет, то доступ есть только к главной
         * странице и к странице регистрации.
         */
        if (request.getRequestURI().contains("account")) {
            if (session.getAttribute("id") == null) {
                response.sendRedirect(request.getContextPath() + "/");
                return false;
            }
        }

        /**
         * Проверяю: если пользователь зашел, то зайти на главную или на страницу
         * регистрации нет возможности.
         *
         * Если пользователь заходил с 'запомнить меня', тогда восставновим сессию
         * из куков.
         */
        if (request.getRequestURI().contains("/welcome") || request.getRequestURI().contains("/signUp")) {
            if (session.getAttribute("id") != null) {
                response.sendRedirect(request.getContextPath() + "/account/user?id=" + session.getAttribute("id"));
                return true;
            } else {
                Cookie[] cookies = request.getCookies();
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("accountId")) {
                        session.setAttribute("id", cookie.getValue());
                        response.sendRedirect(request.getContextPath() + "/account/user?id=" + session.getAttribute("id"));
                        return true;
                    }
                }
            }
        }
        return true;
    }
}
