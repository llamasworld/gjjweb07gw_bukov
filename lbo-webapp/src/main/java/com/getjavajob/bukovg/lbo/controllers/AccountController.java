package com.getjavajob.bukovg.lbo.controllers;

import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.common.model.Message;
import com.getjavajob.bukovg.lbo.common.model.Phone;
import com.getjavajob.bukovg.lbo.common.model.Request;
import com.getjavajob.bukovg.lbo.common.type.Role;
import com.getjavajob.bukovg.lbo.service.dto.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Gleb Bukov on 20.07.16.
 */

@Controller
@MultipartConfig
public class AccountController {

    @Autowired
    private AccountService accountService;

    public static final Logger LOG = LoggerFactory.getLogger(ServiceController.class.getName());

    @RequestMapping(value = "/account/user", method = RequestMethod.GET)
    public ModelAndView showAccountPage(@SessionAttribute("id") int ownerId, @RequestParam("id") int accountId) {

        ModelAndView modelAndView = new ModelAndView("account/accountPage");
        Account account = accountService.getAccountById(accountId);

        if (account != null) {
            modelAndView.addObject("account", account);

            Account owner = accountService.getAccountById(ownerId);
            if (!(account.equals(owner))) {

                Request friendRequest = accountService.findFriendRequestByTwoAccountIds(account.getId(), owner.getId());

                if (friendRequest != null) {
                    String requestStatus = friendRequest.getStatus().toString();
                    switch (requestStatus) {
                        case "ACCEPTED":
                            modelAndView.addObject("relationship", "friends");
                            break;
                        case "REJECTED":
                            modelAndView.addObject("relationship", "REJECTED");
                            break;
                        case "WAITING":
                            modelAndView.addObject("relationship", "WAITING");
                            break;
                    }
                } else {
                    modelAndView.addObject("relationship", "none");
                }
            }
        }

        List<Account> friends = accountService.getAllFriends(accountId);
        modelAndView.addObject("friends", friends);

        List<Message> publicMessages = accountService.getPublicMessagesForAccount(accountId);
        modelAndView.addObject("publicMessages", publicMessages);

        modelAndView.addObject("ownerId", ownerId);

        long totalPagesForPagination = accountService.getTotalPagesForPublicMessagesOfAccount(accountId);
        modelAndView.addObject("totalPagesForPublicMessage", totalPagesForPagination);

        return modelAndView;
    }

    @RequestMapping(value = "/account/image/avatar", method = RequestMethod.GET)
    public void showAvatar(@RequestParam("id") int ownerId, HttpServletResponse response) throws IOException {
        try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(response.getOutputStream())) {
            Account account = accountService.getAccountById(ownerId);
            response.setContentType(account.getContentType());
            bufferedOutputStream.write(account.getPhoto(), 0, account.getPhoto().length);
        }
    }

    @RequestMapping(value = "/account/edit", method = RequestMethod.GET)
    public ModelAndView showEditPage(@SessionAttribute("id") int ownerId) {
        ModelAndView modelAndView = new ModelAndView("/account/editPage");
        modelAndView.addObject("owner", accountService.getAccountById(ownerId));
        return modelAndView;
    }

    @RequestMapping(value = "/account/saveEditAccount", method = RequestMethod.POST)
    public String updateAccount(
            @SessionAttribute("id") int ownerId,
            @RequestParam("photo") MultipartFile photo,
            HttpServletRequest request) throws IOException, ServletException {

        Account account = accountService.getAccountById(ownerId);
        // account.setId(ownerId);
        account.setfName(request.getParameter("fName"));
        account.setlName(request.getParameter("lName"));
        account.setpName(request.getParameter("pName"));
        account.setEmail(request.getParameter("email"));
        account.setSkype(request.getParameter("skype"));
        if (request.getParameter("icq").length() > 0) {
            account.setIcq(Integer.valueOf(request.getParameter("icq")));
        } else {
            account.setIcq(0);
        }
        account.setPassword(request.getParameter("password"));
        account.setRole(Role.valueOf(request.getParameter("role")));

        String[] stringArray = request.getParameterValues("phone");
        if (stringArray != null && stringArray.length > 1) {

            int numberOfPhones = stringArray.length / 3;
            int position = 0;
            List<Phone> phones = new ArrayList<>();

            for (int i = 0; i < numberOfPhones; i++) {
                Phone phone = new Phone();
                phone.setOwner(account);
                if (stringArray[position].length() > 0) {
                    phone.setId(Integer.valueOf(stringArray[position]));
                }
                if (stringArray[position + 1].length() > 0) {
                    phone.setDescription(stringArray[position + 1]);
                }
                phone.setNumber(stringArray[position + 2]);
                position += 3;

                if (phone.getNumber().length() > 0) {
                    phones.add(phone);
                }
            }

            account.setPhones(phones);
        }

        /**
         * Если пользователь загрузил фото тогда мы обновим аватарку,
         * в противном случае мы установим текущую аватарку из сессии.
         *
         * Далее проверим, есть ли галочка 'удалить фото' если есть удалим.
         */
        if (!photo.isEmpty() /*photo != null && photo.getSize() != 0*/) {
            byte[] bytes = photo.getBytes();
            account.setPhoto(bytes);
            account.setContentType(photo.getContentType());
        } else {
            Account owner = accountService.getAccountById(ownerId);
            account.setPhoto(owner.getPhoto());
            account.setContentType(owner.getContentType());
        }

        boolean removeAvatar = Boolean.valueOf(request.getParameter("removeAvatar"));
        if (removeAvatar) {
            account.setPhoto(null);
            account.setContentType(null);
        }
        accountService.updateAccount(account);

        request.getSession(true).setAttribute("ownerfName", account.getfName());
        request.getSession(true).setAttribute("ownerlName", account.getlName());

        LOG.info(account.getfName() + " " + account.getlName() + " has been update account.");

        return "redirect: /account/user?id=" + ownerId;
    }

    @RequestMapping(value = "/account/search")
    @ResponseBody
    public List<Account> searchAccounts(@RequestParam("s") String text) {
        List<Account> searchResult = accountService.findAccountByWord(text);
        return searchResult;
    }
}
