package com.getjavajob.bukovg.lbo.controllers;

import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.common.type.Role;
import com.getjavajob.bukovg.lbo.service.dto.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Gleb Bukov on 20.07.16.
 */

@Controller
public class ServiceController {

    @Autowired
    private AccountService accountService;

    public static final Logger LOG = LoggerFactory.getLogger(ServiceController.class.getName());

    @RequestMapping(value = "/welcome")
    public ModelAndView loginPage() {
        return new ModelAndView("service/login");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(
            @RequestParam("email") String email,
            @RequestParam("password") String password,
            HttpServletRequest request,
            HttpServletResponse response) {

        boolean rememberMe = Boolean.valueOf(request.getParameter("rememberMe"));
        Account account = accountService.getAccountByEmailPassword(email, password);

        if (account != null) {
            if (rememberMe) {
                Cookie cookie = new Cookie("accountId", String.valueOf(account.getId()));
                cookie.setMaxAge(60 * 60 * 24 * 7);
                response.addCookie(cookie);
            }
            HttpSession session = request.getSession(true);
            session.setAttribute("ownerfName", account.getfName());
            session.setAttribute("ownerlName", account.getlName());
            session.setAttribute("id", account.getId());
            LOG.info(account.getfName() + " " + account.getlName() + " has come...");
            return "redirect: /account/user?id=" + account.getId();
        } else {
            LOG.warn("Account '" + email + "' not found");
            return "redirect: /welcome";
        }
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.GET)
    public ModelAndView signupPage(HttpServletRequest request) {
        return new ModelAndView("service/signup");
    }

    @RequestMapping(value = "/createAccount", method = RequestMethod.POST)
    public String signup(HttpServletRequest request) {
        Account account = new Account();
        account.setfName(request.getParameter("fName"));
        account.setlName(request.getParameter("lName"));
        account.setRole(Role.user);
        account.setEmail(request.getParameter("email"));
        account.setPassword(request.getParameter("password"));
        accountService.addNewAccount(account);
        return "redirect: /welcome";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletResponse response, HttpSession session) {

        Cookie cookie = new Cookie("accountId", "0");
        cookie.setMaxAge(0);
        response.addCookie(cookie);

        session.setAttribute("owner", null);
        session.setAttribute("id", null);

        LOG.info("Somebody has gone...");

        return "redirect: /welcome";
    }
}
