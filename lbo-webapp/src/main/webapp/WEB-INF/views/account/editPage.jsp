<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 23.06.16
  Time: 17:48
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="aDating account edit">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/navbar-fixed-top.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/account.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.maskedinput.js"></script>

    <title>Редактирование аккаунта ${owner.fName} ${owner.lName}</title>
</head>
<body>
<%@include file="../support/header.jsp" %>

<div class="container account">
    <div class="row">
        <h3 class="edit-head">Редактирование:</h3>

        <div class="col-md-4 col-md-offset-4">

            <form action="${pageContext.request.contextPath}/account/saveEditAccount" method="post"
                  enctype="multipart/form-data">
                <div class="form-group">
                    <input type="hidden" name="id" value="${owner.id}"/>
                    <input type="hidden" name="role" value="${owner.role}"/>
                    <input type="hidden" name="password" value="${owner.password}"/>

                    <%-- - - - - - - - - - NAME  - - - - - - - - - - --%>
                    <label class="acc-descr-for-edit-filed">Имя</label>
                    <input class="form-control" type="text" name="fName" value="${owner.fName}"/>

                    <%-- - - - - - - - - SURNAME - - - - - - - - - - --%>
                    <label class="acc-descr-for-edit-filed">Фамилия</label>
                    <input class="form-control" type="text" name="lName" value="${owner.lName}"/>

                    <%-- - - - - - - - - PRONNAME  - - - - - - - - - --%>
                    <label class="acc-descr-for-edit-filed">Отчество</label>
                    <c:if test="${owner.pName != null}">
                    <input class="form-control" type="text" name="pName" value="${owner.pName}"/>
                    </c:if>
                    <c:if test="${owner.pName == null}">
                    <input class="form-control" type="text" placeholder="Отчество" name="pName"/>
                    </c:if>

                    <%-- - - - - - - - - - EMAIL  - - - - - - - - - - --%>
                    <label class="acc-descr-for-edit-filed">Электронная почта</label>
                    <input class="form-control" type="text" name="email" value="${owner.email}"/>

                    <%-- - - - - - - - - - SKYPE  - - - - - - - - - - --%>
                    <label class="acc-descr-for-edit-filed">Skype</label>
                    <c:if test="${owner.skype != null}">
                    <input class="form-control" type="text" name="skype" value="${owner.skype}"/>
                    </c:if>
                    <c:if test="${owner.skype == null}">
                    <input class="form-control" type="text" placeholder="Skype" name="skype"/>
                    </c:if>

                    <%-- - - - - - - - - -  ICQ  - - - - - - - - - - --%>
                    <label class="acc-descr-for-edit-filed">ICQ</label>
                    <c:if test="${owner.icq != 0}">
                    <input class="form-control" type="text" name="icq" value="${owner.icq}"/>
                    </c:if>
                    <c:if test="${owner.icq == 0}">
                    <input class="form-control" type="text" placeholder="ICQ" name="icq"/>
                    </c:if>

                    <%-- - - - - - - - - - PHONES  - - - - - - - - - --%>
                    <div id="phones">
                        <hr style="margin-bottom: 5px; margin-top: 25px;"/>
                        <label class="acc-descr-for-edit-filed">Телефоны</label>
                        <c:forEach items="${owner.phones}" var="phone">
                            <div style="padding-bottom: 5px;">
                                <input type="hidden" name="phone" value="${phone.id}"/>
                                    <%--<label class="acc-descr-for-edit-filed">Описание</label>--%>
                                <input class="edit-phone-description form-control" type="text" name="phone"
                                       value="${phone.description}"/>
                                    <%--<label class="acc-descr-for-edit-filed">Номер</label>--%>
                                <input id="phone_number" class="edit-phone-number form-control" type="text" name="phone"
                                       value="${phone.number}"/>
                                <button id="remove_phone" type="button"
                                        class="edit-delete-phone-btn btn btn-danger">&ndash;</button>
                            </div>
                        </c:forEach>
                    </div>
                    <div style="text-align: center; padding-top: 15px;">
                        <button id="add_phone" type="button" class="edit-add-phone-btn btn btn-success btn-xs">Добавить телефон</button>
                    </div>
                    <hr style="margin-bottom: 5px; margin-top: 20px;"/>

                    <%-- - - - - - - - - - AVATAR  - - - - - - - - - --%>
                    <label class="acc-descr-for-edit-filed">Фотография</label>
                    <input type="file" name="photo" <%--size="50"--%>/>
                    <div class="checkbox edit-check-box">
                        <label class="acc-descr-for-edit-filed">
                            <input type="checkbox" name="removeAvatar" value="true"/> Удалить фотографию
                        </label>
                    </div>
                    <input class="btn btn-default edit-save-btn" type="submit" name="Save" value="Сохранить">
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>

<%@include file="../support/scriptCellar.jsp" %>
</body>
</html>
