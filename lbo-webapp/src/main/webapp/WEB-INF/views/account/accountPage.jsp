<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 23.06.16
  Time: 0:39
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="aDating account">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/navbar-fixed-top.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/account.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>

    <title>Страница пользователя ${owner.fName} ${owner.lName}</title>
</head>
<body>

<%@include file="../support/header.jsp" %>

<div class="container account-container-fix">

    <div class="row account-row">

        <%-- - - - - - - - - - - - - - - - - - - - - - - Contact block - - - - - - - - - - - - - - - - - - - - - - --%>
        <div class="col-md-4 accavatar">
            <c:if test="${account.contentType != null}">
                <img src="${pageContext.request.contextPath}/account/image/avatar?id=${account.id}"/>
            </c:if>
            <c:if test="${account.contentType == null}">
                <c:if test="${account.id == ownerId}">
                    <div>
                        <a class="accuploadphototext" href="${pageContext.request.contextPath}/account/edit">Загрузить
                            фото</a>
                    </div>
                </c:if>
                <img src='https://s31.postimg.org/rj6jlt3qz/placeholder_Photo_2.png'/>
            </c:if>
        </div>

        <div class="col-md-8">
            <h3>
                ${account.lName} ${account.fName} ${account.pName}

                <%-- Проверяем отношения между owner и account --%>
                <c:if test="${account.id != ownerId}" var="friend" scope="page">

                    <c:if test="${relationship == 'friends'}">
                        <span class="acc-friend-status" style="color:green;">друзья</span>
                    </c:if>

                    <c:if test="${relationship == 'rejected'}">
                        <span class="acc-friend-status" style="color:red;">враги</span>
                    </c:if>

                    <c:if test="${relationship == 'waiting'}">
                        <span class="acc-friend-status">ожидаем ответа</span>
                    </c:if>

                    <c:if test="${relationship == 'none'}">
                        <a class="acc-friend-status"
                           href="${pageContext.request.contextPath}/account/friends/add?friendId=${account.id}">
                            добавить в друзья
                        </a>
                    </c:if>

                </c:if>
            </h3>

            <hr>

            <c:if test="${account.skype != null}" var="isSkype" scope="page">
                <p>skype - ${account.skype}</p>
            </c:if>
            <c:if test="${account.icq != null}">
                <c:if test="${account.icq != 0}" var="isIcq" scope="page">
                    <p>icq - ${account.icq}</p>
                </c:if>
            </c:if>

            <c:forEach items="${account.phones}" var="phone">
                <c:if test="${phone.description != null}">
                    <p>${phone.description} - ${phone.number}</p>
                </c:if>
                <c:if test="${phone.description == null}">
                    <p>Телефон - ${phone.number}</p>
                </c:if>
            </c:forEach>
        </div>

    </div>

    <hr class="acc-hr-between-module"/>

    <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - Friends - - - - - - - - - - - - - - - - - - - - - - - --%>
    <div class="row">
        <div class="col-md-4" style="background-color: #f8f8f8; width: 300px; padding-bottom: 30px;">

            <c:if test="${fn:length(friends) == 0}">
                <h4 style="text-align: center">Кажется вам стоит добавить друзей!</h4>
            </c:if>

            <h3 style="text-align: center;">Друзья</h3>
            <hr style="margin-bottom: -10px;"/>
            <c:forEach items="${friends}" var="friend">
                <div class="col-md-6" style="text-align: center; padding-bottom: 15px;">
                    <c:if test="${friend.contentType != null}">
                        <img src="${pageContext.request.contextPath}/account/image/avatar?id=${friend.id}"
                             class="account-avatar-for-friend-cell"/>
                    </c:if>
                    <c:if test="${friend.contentType == null}">
                        <img src="https://s31.postimg.org/rj6jlt3qz/placeholder_Photo_2.png"
                             class="account-avatar-for-friend-cell"/>
                    </c:if>
                    <br/>
                    <a href="${pageContext.request.contextPath}/account/user?id=${friend.id}">${friend.fName} ${friend.lName}</a>
                </div>
            </c:forEach>

            <hr class="acc-hr-between-module" style="width: 150%; border-top: 20px solid #eee;"/>

            <h3 style="text-align: center;">Группы</h3>
            <hr style="margin-bottom: -10px;"/>
            <c:forEach items="${account.groups}" var="group">
                <div class="col-md-12" style="text-align: center;">
                    <img src="https://s31.postimg.org/5k0d1a3l7/placeholder_Photo_3_1.gif"
                         class="account-group-avatar"/>
                    <p><a href="${pageContext.request.contextPath}/account/group?id=${group.id}">${group.name}</a></p>
                    <p class="groupDescriptionAccountPage">${group.description}</p>
                </div>
            </c:forEach>

        </div>
        <%-- - - - - - - - - - - - - - - - - - - - - - - - - - - TheWall - - - - - - - - - - - - - - - - - - - - - - - --%>
        <div class="col-md-7" style="margin-left: 20px; width: 650px; background-color: #f8f8f8;">

            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-8 col-md-offset-2">
                    <h3 style="text-align: center;">Публичные сообщения</h3>
                </div>
                <div class="col-md-2">
                    <h3>
                        <button id="openPublicMessageToAccountBox" style="border: 0px; background-color: transparent;">
                            <span class="account-glyph-public-msg glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                    </h3>
                </div>
            </div>

            <div id="publicMessageAccountBox" class="account-public-msg-div row" style="display: none;">
                <div class="col-md-2" style="padding-top: 20%;">
                    <p style="transform: rotate(-90deg) scale(1, 1.3); color: #f6f6f6; font-size: 2em;">
                        НАПИСАТЬ</p>
                </div>
                <div class="col-md-10">
                    <form action="${pageContext.request.contextPath}/account/messages/public/send" method="post">
                        <input type="hidden" name="toAccountId" value="${account.id}"/>
                        <textarea class="form-control" name="publicMessage" rows="5" style="width: 100%"></textarea>
                        <input class="messages-account-send-btn btn btn-default" type="submit" value="Отправить">
                        <input id="closePublicMessageBoxToAccount" class="messages-account-send-btn btn btn-default"
                               value="Отменить">
                    </form>
                </div>
            </div>

            <div id="accountPublicMessageBox">
                <%-- place for public messages --%>
            </div>

            <nav aria-label="Page navigation" style="text-align: center;">

                <%-- Information for ajax script. Don't remove it. --%>
                <span class="paginationButtonStart" style="display: none;">1</span>
                <span id="accountIdForPagination" style="display: none">${account.id}</span>
                <span id="ownerIdForPagination" style="display: none">${ownerId}</span>

                <ul class="pagination">
                    <c:forEach var="i" begin="1" end="${totalPagesForPublicMessage}">
                        <li><a class="paginationButton" href="#anchorForPagination"><c:out value="${i}" /></a></li>
                    </c:forEach>
                </ul>
            </nav>

        </div>
    </div>
</div>

<%@include file="../support/scriptCellar.jsp" %>

</body>
</html>

