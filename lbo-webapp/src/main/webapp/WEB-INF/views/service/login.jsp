<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 20.06.16
  Time: 18:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="aDating login">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <title>aDating - соединяем противоположности.</title>

    <!-- Bootstrap core CSS -->
    <link href="<c:url value='/resources/css/bootstrap.css'/>" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- Custom styles for this template -->
    <%--<link href="signin.css" rel="stylesheet">--%>
    <%--<link href="${pageContext.request.contextPath}/resources/css/signin.css" rel="stylesheet">--%>
    <link href="${pageContext.request.contextPath}/resources/css/welcome.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <form class="form-signin" action="${pageContext.request.contextPath}/login" method="post">
        <img style="padding-bottom: 10px;" src="${pageContext.request.contextPath}/resources/img/aDatingLogo.png">
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email" name="email" required autofocus />
        <label for="inputPassword" class="sr-only">Пароль</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Пароль" name="password" required />
        <div class="checkbox">
            <label>
                <input type="checkbox" name="rememberMe" value="true" /> Запомнить меня
            </label>
        </div>
        <input class="btn btn-lg btn-primary btn-block" type="submit" name="enter" value="Войти" />
        <div style="padding-top: 10px">
            <a style="padding-top: 10px;" href="/signUp">Регистрация</a>
        </div>
    </form>

</div> <!-- /container -->

</body>
</html>


<%--<html>
<head>
    <link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/css/main.css' />
    <title>Welcome to aDating</title>
</head>
<body>
    <div class="welcomeMainDiv">
        <div class="welcomeDiv"><h1 class="welcomeH1">Time for</h1></div>
        <div class="welcomeDiv"><img style="padding-left: 20px; padding-right: 20px;" src="img/logo-1.png"></div>
        <div class="welcomeDiv"><h1 class="welcomeH1"><i style="color: #d57236;">a</i>Dating!</h1></div>
    </div>
    <div style="padding-top: 35px; text-align: center;">
        <form action="${pageContext.request.contextPath}/login" method="post">
            <p style="padding-left: 8%;">
                <input class="inputsUP" type="text" required placeholder="Email" name="email" />
                <input class="inputsUP" type="text" required placeholder="Пароль" name="password" />
                <input type="checkbox" name="rememberMe" value="true" />
                <span style="font-size: 0.7em; color: #6d6455;">
                    Запомнить меня
                </span>
            </p>
            <p>
                <input class="btn" type="submit" name="enter" value="Войти" />
            </p>
        </form>
        <p>
            <a href="${pageContext.request.contextPath}/signUp">
                <span style="color: #6d6455; font-size: 0.7em; margin-left: -10px;">Регистрация</span>
            </a>
        </p>
        &lt;%&ndash;<a href="login.jsp"><span class="btn">Войти</span></a>&ndash;%&gt;
    </div>
</body>
</html>--%>
