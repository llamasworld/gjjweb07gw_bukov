<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 21.06.16
  Time: 9:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="aDating signUp">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- Custom styles for this template -->
    <%--<link href="${pageContext.request.contextPath}/resources/css/signup.css" rel="stylesheet">--%>
    <link href="${pageContext.request.contextPath}/resources/css/welcome.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>

    <title>Регистрация</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-body">
                <form method="POST" action="${pageContext.request.contextPath}/createAccount" role="form">
                    <div class="form-group">
                        <h2>Регистрация</h2>
                    </div>

                    <%-- - - - - - - - - - NAME  - - - - - - - - - - --%>
                    <div class="form-group">
                        <label class="control-label" for="signupName">Имя</label>
                        <span id="nameWarning" style="padding-left: 5px;"></span>
                        <input id="signupName" type="text" maxlength="50" class="form-control" name="fName"
                               onchange="showNamesWarning(this, $('#nameWarning'))" required>
                    </div>

                    <%-- - - - - - - - - - SURNAME - - - - - - - - - --%>
                    <div class="form-group">
                        <label class="control-label" for="signupSurname">Фамилия</label>
                        <span id="surnameWarning" style="letter-spacing: -0.8px;"></span>
                        <input id="signupSurname" type="text" maxlength="50" class="form-control" name="lName"
                               onchange="showNamesWarning(this, $('#surnameWarning'))" required>
                    </div>

                    <%-- - - - - - - - - - EMAIL - - - - - - - - - - --%>
                    <div class="form-group">
                        <label class="control-label" for="signupEmail">Электронная почта</label>
                        <span id="emailWarning"></span>
                        <input id="signupEmail" type="email" maxlength="50" class="form-control" name="email"
                               onchange="showEmailWarning(this);" required>
                    </div>

                    <%-- - - - - - - - - - PASSWORD  - - - - - - - - --%>
                    <div class="form-group">
                        <label class="control-label" for="signupPassword">Пароль</label>
                        <span id="passwordWarning"></span>
                        <input id="signupPassword" type="password" maxlength="25" class="form-control" name="password"
                               placeholder="Минимум 6 символов" oninput="showPasswordWarning(this)"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="signupPasswordDuplicate">Повторите пароль</label>
                        <span id="passwordWarningDuplicate"></span>
                        <input id="signupPasswordDuplicate" type="password" maxlength="25" class="form-control"
                               oninput="showPasswordWarningDuplicate(this)">
                    </div>

                    <%-- - - - - - - - - - SUBMIT - - - - - - - - - --%>
                    <div class="form-group">
                        <button id="signupSubmit" type="submit" class="btn btn-lg btn-primary btn-block">Создать
                            аккаунт
                        </button>
                    </div>

                    <%-- - - - - - - - -  LICENSE - - - - - - - - - --%>
                    <p class="form-group">Создавая аккаунт вы соглашаетесь с нашей <a href="#">Политикой
                        конфиденциальности</a> и нашими <a href="#">Правилами</a>.</p>
                    <hr>
                    <p></p>Уже есть аккаунт? <a href="/">Войти</a></p>
                </form>
            </div>
        </div>
    </div>
</div> <!-- /container -->

<%@include file="../support/scriptCellar.jsp" %>

</body>
</html>

<%--<html>
<head>
    <link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/css/main.css' />
    <title>Регистрация</title>
</head>
<body>
    <div class="registrationMainDiv">
        <h2 class="registrH2">Регистрация</h2>
        <form action="${pageContext.request.contextPath}/createAccount" method="post">
            <p><input class="registInput" type="text" required placeholder="Имя" name="fName" /></p>
            <p><input class="registInput" type="text" required placeholder="Фамилия" name="lName" /></p>
            <p><input class="registInput" type="text" required placeholder="Email" name="email" /></p>
            <p><input class="registInput" type="text" required placeholder="Пароль" name="password" /></p>
            <p><input class="registrBtn" type="submit" name="save" value="Регистрация" /></p>
        </form>
    </div>
</body>
</html>--%>
