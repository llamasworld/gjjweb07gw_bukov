<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 03.08.16
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="aDating groupOverview">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/navbar-fixed-top.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/account.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>

    <title>Обзор групп ${owner.fName} ${owner.lName}</title>
</head>
<body>
<%@include file="../support/header.jsp" %>

<div class="container account">

    <div class="row">
        <h3 class="edit-head">Сообщества</h3>
        <hr class="page-header-separate"/>

        <%-- - - - - - - - - - - - - - - - - - - - - - - - - Search - - - - - - - - - - - - - - - - - - - - - - - - --%>

        <div class="row">
            <div class="col-md-8 col-md-offset-2"
                 style="text-align: center; margin-bottom: 2.5%; background-color: #9cc394">
                <form class="form-inline" style="padding-top: 2.2%;">
                    <div class="form-group has-success has-feedback" style="width: 100%;">
                        <input type="text" class="input2 form-control group-search-input" id="searchGroupInput"
                               aria-describedby="inputSuccess4Status" placeholder="    Поиск сообществ">
                        <span class="group-search-glyphicon glyphicon glyphicon-search form-control-feedback"
                              aria-hidden="true"></span>
                    </div>
                    <div id="searchResultForGroups" class="group-search-result-div">
                        <%-- search result here --%>
                    </div>
                </form>
            </div>

            <div class="col-md-8 col-md-offset-2" style="text-align: center; margin-bottom: 2.5%;">
                <button id="createGroupBtn" type="button" class="btn btn-warning btn-lg btn3d">
                    <span class="glyphicon glyphicon-plus"></span> Создать сообщество
                </button>
            </div>

        </div>

        <hr class="acc-hr-between-module"/>

        <%-- - - - - - - - - - - - - - - - - - - - - - - - - List - - - - - - - - - - - - - - - - - - - - - - - - - --%>

        <div class="row" id="groupList">
            <div class="col-md-12">
                <div class="tabbable-panel">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_all_groups" data-toggle="tab">Все сообщества</a>
                            </li>
                            <li>
                                <a href="#tab_owners_groups" data-toggle="tab">Ваши сообщества</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_all_groups">

                                <c:if test="${fn:length(groups) == 0}">
                                    <h3 style="text-align: center;">Как-то у вас тут скучно :( добавь группы!</h3>
                                </c:if>

                                <c:forEach items="${groups}" var="group">

                                    <div class="row">
                                        <div class="col-md-4" style="padding-right: 0px;">
                                            <img src='https://s31.postimg.org/5k0d1a3l7/placeholder_Photo_3_1.gif'/>
                                        </div>
                                        <div class="col-md-8">
                                            <h3 style="font-size: 1.4em;">
                                                <a href="${pageContext.request.contextPath}/account/group?id=${group.id}">
                                                        ${group.name}
                                                </a>
                                            </h3>
                                            <p>${group.description}</p>

                                            <span class="group-stats-glyphicon-first glyphicon glyphicon-user"></span>
                                            <span>${fn:length(group.members)}</span>

                                            <span class="group-stats-glyphicon glyphicon glyphicon-comment"></span>
                                            <span style="padding-left: 3%; padding-right: 4%;">ХХХ</span>

                                            <span class="group-stats-glyphicon glyphicon glyphicon-remove"></span>
                                            <a href="${pageContext.request.contentType}/account/group/leave?groupId=${group.id}"
                                               style="padding-left: 3%;">выйти из сообщества</a>
                                        </div>
                                    </div>
                                    <hr/>

                                </c:forEach>

                            </div>
                            <div class="tab-pane" id="tab_owners_groups">
                                <p style="text-align: center">
                                    Work in progress...
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

        <%-- - - - - - - - - - - - - - - - - - - - - - - -  Create  - - - - - - - - - - - - - - - - - - - - - - - - --%>

    <div class="row" id="createGroupDiv" style="display: none">
        <div class="col-md-6 col-md-offset-3" style="margin-top: 20px;">
            <div class="panel panel-primary" style="border-color: #9cc394;">
                <div class="panel-body">
                    <form method="POST" action="${pageContext.request.contextPath}/account/createGroup" role="form">
                        <div class="form-group">
                            <h2 style="text-align: center;">Создание сообщества</h2>
                        </div>

                        <%-- - - - - - - - - - NAME  - - - - - - - - - - --%>
                        <div class="form-group">
                            <label class="control-label" for="groupName">Название</label>
                            <input id="groupName" type="text" maxlength="50" class="form-control" name="groupName"
                                   required>
                        </div>

                        <%-- - - - - - - - - - DESCRITION- - - - - - - - --%>
                        <div class="form-group">
                            <label class="control-label">Описание</label>
                            <textarea class="form-control" name="description" rows="3" style="width: 100%"></textarea>
                        </div>

                        <%-- - - - - - - - - - SUBMIT - - - - - - - - - --%>
                        <div class="form-group" style="text-align: center;">
                            <button id="createGroupSubmit" type="submit"
                                    class="group-btn-create btn btn-lg btn-primary btn-block">
                                Создать сообщество
                            </button>
                            <a id="cancelCreateGroup" href="#">Я передумал</a>
                        </div>

                        <%-- - - - - - - - -  LICENSE - - - - - - - - - --%>
                        <p class="form-group">Создавая сообщество вы соглашаетесь с нашими <a href="#">Правилами</a>.
                        </p>
                        <hr>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%@include file="../support/scriptCellar.jsp" %>
</div>
</body>
</html>