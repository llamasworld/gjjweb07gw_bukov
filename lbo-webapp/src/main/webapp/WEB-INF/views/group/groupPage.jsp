<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 05.08.16
  Time: 11:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="aDating group page">
        <meta name="author" content="Gleb Bukov">
        <%--<link rel="icon" href="../../favicon.ico">--%>

        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/resources/css/navbar-fixed-top.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/resources/css/account.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Scripts for this page -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>

        <title>Обзор группы Name</title>
    </head>
<body>
<%@include file="../support/header.jsp" %>

<div class="container account">

    <div class="row">
        <h3 class="edit-head">Сообщества</h3>
        <hr class="page-header-separate"/>

        <%-- - - - - - - - - - - - - - - - - - - - - - Name + description - - - - - - - - - - - - - - - - - - - - - --%>
        <div class="row">
            <div class="col-md-8 col-md-offset-2"
                 style="text-align: center; margin-bottom: 2.5%;">
                <h2>${group.name}</h2>
                <hr/>
                <h4 style="font-weight: normal;">${group.description}</h4>
            </div>

            <c:if test="${guest == null}">
                <div class="col-md-8 col-md-offset-2" style="text-align: center; margin-bottom: 2.5%;">
                    <a id="joinToGroup" <%--type="button"--%> class="btn btn-warning btn-lg btn3d"
                       href="/account/group/join?groupId=${group.id}">
                        <span class="glyphicon glyphicon-plus"></span> Вступить
                    </a>
                </div>
            </c:if>
        </div>

        <hr class="acc-hr-between-module"/>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <span style="padding-left: 7%;">Администратор:</span><a
                    href="#"> ${group.owner.lName} ${group.owner.fName}</a>
                <a href="${pageContext.request.contentType}/account/group/leave?groupId=${group.id}"
                   style="border-left: 1px solid; padding-left: 15px; margin-left: 15px; color: #cf524b;">
                    Покинуть группу
                </a>
                <a href="#"
                   style="text-decoration: line-through; border-left: 1px solid; padding-left: 15px; margin-left: 15px;">
                    Пожаловаться
                </a>
            </div>
        </div>

        <hr class="acc-hr-between-module"/>

        <%-- - - - - - - - - - - - - - - - - - - - - - - -  Members - - - - - - - - - - - - - - - - - - - - - - - - --%>
        <div class="row">
            <c:forEach items="${group.members}" var="member" begin="0" end="4">
                <div class="col-md-2" style="text-align: center; margin-bottom: 1.5%; margin-top: 1.5%;">
                    <c:if test="${member.contentType != null}">
                        <a href="/account/user?id=${member.id}">
                            <img src="${pageContext.request.contextPath}/account/image/avatar?id=${member.id}"
                                 style="width: 100px;"/>
                        </a>
                    </c:if>
                    <c:if test="${member.contentType == null}">
                        <a href="/account/user?id=${member.id}">
                            <img src="https://s31.postimg.org/rj6jlt3qz/placeholder_Photo_2.png" style="width: 100px;"/>
                        </a>
                    </c:if>
                </div>
            </c:forEach>
            <div class="col-md-1" style="text-align: center; margin-bottom: 2.5%;">
                <div style="text-align: center; padding-top: 80%; padding-left: 30;">
                    <a href="#" style="text-decoration: line-through">Все</a>
                    <a href="#" style="text-decoration: line-through">участники</a>
                </div>
            </div>
        </div>

        <hr class="acc-hr-between-module"/>

        <%-- - - - - - - - - - - - - - - - - - - - - - - -  Messages  - - - - - - - - - - - - - - - - - - - - - - - --%>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 style="text-align: center; font-weight: normal;">Написать сообщение</h3>
                    </div>
                    <div class="col-md-2">
                        <h3>
                            <button id="openPublicMessageToAccountBox"
                                    style="border: 0px; background-color: transparent;">
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"
                                      style="margin-left: -700%; font-size: 1.1em; margin-top: 1px;"></span>
                            </button>
                        </h3>
                    </div>
                </div>

                <div id="publicMessageAccountBox" class="account-public-msg-div row">
                    <div class="col-md-2" style="padding-top: 16%;">
                        <p style="transform: rotate(-90deg) scale(1, 1.3); color: #f6f6f6; font-size: 2em;">
                            НАПИСАТЬ</p>
                    </div>
                    <div class="col-md-10">
                        <form action="${pageContext.request.contextPath}/account/group/messages/public/send"
                              method="post">
                            <input type="hidden" name="toGroupId" value="${group.id}"/>
                            <textarea class="form-control" name="publicMessage" rows="3" style="width: 100%"></textarea>
                            <input class="messages-account-send-btn btn btn-default" type="submit" value="Отправить">
                            <input id="closePublicMessageBoxToAccount" class="messages-account-send-btn btn btn-default"
                                   value="Отменить">
                        </form>
                    </div>
                </div>

                <h3 style="text-align: center; font-weight: normal; padding-bottom: 10px;">Сообщения сообщества</h3>
                <c:forEach items="${messages}" var="message">
                    <div class="account-public-msg-div row">
                        <div class="col-md-2">
                            <c:if test="${message.fromAcc.contentType != null}">
                                <a href="/account/user?id=${member.id}">
                                    <img src="${pageContext.request.contextPath}/account/image/avatar?id=${message.fromAcc.id}"
                                         style="width: 100px;"/>
                                </a>
                            </c:if>
                            <c:if test="${message.fromAcc.contentType == null}">
                                <a href="/account/user?id=${member.id}">
                                    <img src="https://s31.postimg.org/rj6jlt3qz/placeholder_Photo_2.png"
                                         style="width: 100px;"/>
                                </a>
                            </c:if>
                        </div>
                        <div class="col-md-10" style="padding-left: 7%;">
                            <p>Пользователь
                                <a href="${pageContext.request.contextPath}/account/user?id=${message.fromAcc.id}">${message.fromAcc.fName} ${message.fromAcc.lName}</a>
                                пишет:
                            </p>
                            <hr/>
                            <p>${message.description}</p>
                        </div>
                        <c:if test="${(message.fromAcc.id == id) || (group.owner.id == id)}">
                            <div style="margin-top: 18%;">
                                <hr style="margin-bottom: 10px;"/>
                                <a href="${pageContext.request.contextPath}/account/group/messages/public/remove?messageId=${message.id}&groupId=${group.id}"
                                   style="color: #cf524b;">
                                    Удалить сообщение
                                </a>
                                <a href="#" style="padding-left: 9%; text-decoration: line-through;">Ответить</a>
                            </div>
                        </c:if>
                    </div>
                </c:forEach>
            </div>
        </div>

    </div>

    <%@include file="../support/scriptCellar.jsp" %>
</div>
</body>
</html>
