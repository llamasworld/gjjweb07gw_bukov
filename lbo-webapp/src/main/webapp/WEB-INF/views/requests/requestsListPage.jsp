<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 09.07.16
  Time: 23:11
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="aDating requests">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/navbar-fixed-top.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/account.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>

    <title>Уведомления пользователя ${owner.fName} ${owner.lName}</title>
</head>
<body>
<%@include file="../support/header.jsp" %>

<div class="container account">

    <div class="row">
        <h3 class="edit-head">Уведомления</h3>
        <hr class="page-header-separate"/>

        <%-- TODO А что будет если есть реквесты, которые были ACCEPTED and REJECTED ;-) --%>
        <c:if test="${fn:length(requests) == 0}">
            <h4 class="acc-head-for-empty-page">Пока все тихо!</h4>
        </c:if>

        <c:forEach items="${requests}" var="request">
            <c:if test="${request.status == 'WAITING'}">
                <div class="requestCol col-md-6">
                    <div class="requestCell bg-success">
                        <p class="request-cell-header">Запрос на дружбу от
                            <a href="${pageContext.request.contextPath}/account/user?id=${request.fromAcc.id}">
                                    ${request.fromAcc.lName} ${request.fromAcc.fName}
                            </a>
                        </p>
                        <p>Статус заявки: <span style="color:#a7ab25;">ожидание</span></p>
                        <div class="col-md-2" style="padding: 0px;">
                            <form class="request-form-inline-fix form-inlinee"
                                  action="${pageContext.request.contextPath}/account/requests/accept" method="post">
                                <input type="hidden" name="requestId" value="${request.id}"/>
                                <input class="btn btn-success" style="margin-right: 5px;" type="submit" name="accept"
                                       value="Принять"/>
                            </form>
                        </div>
                        <div class="col-md-2">
                            <form class="request-form-inline-fix form-inlinee"
                                  action="${pageContext.request.contextPath}/account/requests/reject" method="post">
                                <input type="hidden" name="requestId" value="${request.id}"/>
                                <input class="btn btn-danger" onclick="return confirmRejectFriendRequest();"
                                       type="submit" name="reject" value="Отклонить"/>
                            </form>
                        </div>
                    </div>
                </div>
            </c:if>
        </c:forEach>

    </div>

    <%@include file="../support/scriptCellar.jsp" %>
</body>
</html>
