<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 09.07.16
  Time: 23:11
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="aDating requests">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/navbar-fixed-top.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/account.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>

    <title>Сообщения пользователя ${ownerfName} ${ownerlName}</title>
</head>
<body>
<%@include file="../support/header.jsp" %>

<div class="container account">

    <div class="row">
        <h3 class="edit-head">Общение</h3>
        <hr class="page-header-separate"/>

        <div style="text-align: center;">
            <c:forEach items="${friends}" var="friend">
                <div class="bs-calltoaction bs-calltoaction-success">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 cta-contents" style="text-align: left;">
                            <a class="cta-title"
                               href="${pageContext.request.contextPath}/account/user?id=${friend.id}">${friend.lName} ${friend.fName}</a>
                        </div>
                        <div class="col-md-3 cta-contents" style="text-align: right;">
                            <div class="cta-desc">
                                <%-- TODO counting for messages --%>
                                <p>Сообщений: ***</p>
                            </div>
                        </div>
                        <div class="col-md-3 cta-button">
                            <a href="${pageContext.request.contextPath}/account/messages/conversation/${friend.id}"
                               class="btn btn-lg btn-block btn-warning" style="border-radius: 5px;">Болтать!</a>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>

    </div>

    <%@include file="../support/scriptCellar.jsp" %>
</div>
</body>
</html>
