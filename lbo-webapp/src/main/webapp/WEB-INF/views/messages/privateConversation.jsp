<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 19.07.16
  Time: 18:16
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="aDating private conversation">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/navbar-fixed-top.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/account.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.js"></script>

    <title>Беседа пользователя ${ownerfName} ${ownerlName}</title>
</head>
<body>
<%@include file="../support/header.jsp" %>

<div class="container account">

    <div class="row">
        <h3 class="edit-head">Общение с ${opponent.fName} ${opponent.lName}</h3>
        <hr class="page-header-separate"/>

        <div class="col-md-3" style="text-align: center;">

            <c:if test="${opponent.contentType != null}">
                <img src="${pageContext.request.contextPath}/account/image/avatar?id=${opponent.id}"
                     class="messages-account-avatar"/>
            </c:if>
            <c:if test="${opponent.contentType == null}">
                <img src="https://s31.postimg.org/rj6jlt3qz/placeholder_Photo_2.png"
                     class="messages-account-avatar"/>
            </c:if>
            <br/>
            <a href="${pageContext.request.contextPath}/account/user?id=${opponent.id}">${opponent.fName} ${opponent.lName}</a>

        </div>

        <div class="col-md-6">
            <div id="messageDiv" class="messages-account-msg-frame">
                <c:forEach items="${messages}" var="message">

                    <%-- TODO Data formatting --%>
                    <c:if test="${message.fromAcc.id == owner.id}">
                    <div class="messages-account-right-msg col-md-7 col-md-offset-5">
                            ${message.fromAcc.fName}<br/>
                            ${message.date}<br/>
                            ${message.description}<br/>
                    </div>
                    </c:if>
                    <c:if test="${message.fromAcc.id != owner.id}">
                    <div class="messages-account-left-msg col-md-7">
                            ${message.fromAcc.fName}<br/>
                            ${message.date}<br/>
                            ${message.description}<br/>
                    </div>
                    </c:if>

                </c:forEach>
            </div>

            <%-- TODO Fix send empty message --%>
            <div>
                <form action="${pageContext.request.contextPath}/account/messages/conversation/send" method="post">
                    <input type="hidden" name="opponentId" value="${opponent.id}"/>
                    <textarea class="form-control" name="description" rows="3" style="width: 100%"></textarea>
                    <input class="messages-account-send-btn btn btn-default" type="submit" value="Отправить">
                </form>
            </div>
        </div>

        <div class="col-md-3" style="text-align: center;">

            <c:if test="${owner.contentType != null}">
                <img src="${pageContext.request.contextPath}/account/image/avatar?id=${owner.id}"
                     class="messages-account-avatar"/>
            </c:if>
            <c:if test="${owner.contentType == null}">
                <img src="https://s31.postimg.org/rj6jlt3qz/placeholder_Photo_2.png"
                     class="messages-account-avatar"/>
            </c:if>
            <br/>
            <a href="${pageContext.request.contextPath}/account/user?id=${owner.id}">${owner.fName} ${owner.lName}</a>

        </div>

    </div>

    <%@include file="../support/scriptCellar.jsp" %>
</body>
</html>
