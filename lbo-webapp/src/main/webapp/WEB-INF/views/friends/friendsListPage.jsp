<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 05.07.16
  Time: 17:06
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="aDating friends">
    <meta name="author" content="Gleb Bukov">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <%--<link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">--%>

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/navbar-fixed-top.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/account.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>

    <title>Друзья пользователя ${owner.fName} ${owner.lName}</title>
</head>
<body>
<%@include file="../support/header.jsp" %>

<div class="container account">

    <div class="row">
        <h3 class="edit-head">Друзья</h3>
        <hr class="page-header-separate"/>

        <c:if test="${fn:length(friends) == 0}">
            <h4 class="acc-head-for-empty-page">Возможно вам стоит <a href="">добавить</a> новых друзей!</h4>
        </c:if>

        <c:forEach items="${friends}" var="friend">
            <div class="friendCol col-md-4">
                <div class="friendCell bg-info">
                    <p>
                        <a href="${pageContext.request.contextPath}/account/user?id=${friend.id}">
                                ${friend.lName} ${friend.fName} ${friend.pName}
                        </a>
                    </p>
                    <a class="friendDelete"
                       href="${pageContext.request.contextPath}/account/friends/remove?friendId=${friend.id}">удалить</a>
                </div>
            </div>
        </c:forEach>
    </div>

    <%@include file="../support/scriptCellar.jsp" %>
</div>
</body>
</html>
