<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 27.06.16
  Time: 21:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <%--<a class="navbar-brand" href="#">Project name</a>--%>
            <img style="padding-right: 31px;"
                 src="${pageContext.request.contextPath}/resources/img/aDatingSmallLogoForHeader.png"/>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

            <%-- - - - - - - - - - - - - - - - - - - - Main menu (left side)  - - - - - - - - - - - - - - - - - - - --%>
            <ul class="nav navbar-nav">
                <li><a href="/account/user?id=${id}">Главная</a></li>
                <li><a href="<c:url value='/account/friends'/>">Друзья</a></li>
                <li><a href="<c:url value='/account/messages'/>">Сообщения</a></li>
                <li><a href="<c:url value='/account/groups'/>">Группы</a></li>
                <li><a href="<c:url value='/account/requests'/>">Уведомления</a></li>
            </ul>

            <%-- - - - - - - - - - - - - - - - - - - Live search (central side) - - - - - - - - - - - - - - - - - - --%>
            <ul class="nav navbar-nav">
                <li style="padding-left: 150px;">
                    <form class="header-menu-search-form form-inline">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="header-menu-search-glyph-div input-group-addon">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </div>
                                <input id="searchField" type="text" class="header-menu-search-field form-control" placeholder="Поиск (мин 2 символа)">
                            </div>
                        </div>
                    </form>

                    <div id="searchResult" class="header-menu-search-result-div"></div>
                </li>
            </ul>

            <%-- - - - - - - - - - - - - - - -  - - - Edit and Exit (right side)- - - - - - - - - - - - - - - - - - --%>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">${ownerfName} ${ownerlName} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<c:url value='/account/edit' />">Редактировать</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<c:url value='/logout'/>">Выйти</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<%--<link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/css/main.css' />
    <div id="header">
        <div class="headContainer">
            <p class="headPMenu">
                <a class="headAMenu" href="<c:url value='/account/user?id='/>${owner.id}">Главная</a>
                <span class="headMenuSeparate"> | </span>
                <a class="headAMenu" href="<c:url value='/account/friends'/>">Друзья</a>
                <span class="headMenuSeparate"> | </span>
                <a class="headAMenu" href="<c:url value='/account/edit'/>">Редактировать</a>
                <span style="padding-left: 50%;">
                    Вы вошли как ${owner.fName} ${owner.lName}
                    <a href="<c:url value='/logout'/>">выйти.</a>
                </span>
            </p>
        </div>
    </div>
    <hr style="padding-bottom: 9%; border: 0px;" />--%>
