<%--
  Created by IntelliJ IDEA.
  User: glebb
  Date: 07.07.16
  Time: 19:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/resources/js/jquery.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>