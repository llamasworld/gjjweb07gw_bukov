/**
 * Created by glebb on 13.07.16.
 */

"use strict";

/*-=--------------------------------------------------  P A G E S  -------------------------------------------------=-*/
/*    .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.    .-.
 `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'*/
/*-=-------------------------------------------------  SignUpPage  -------------------------------------------------=-*/
function showEmailWarning(inputElem) {
    if (checkEmail(inputElem.value)) {
        $("#emailWarning").css({
            color: "green",
            paddingLeft: ""
        });
        $("#emailWarning").text("✓");
    } else {
        $("#emailWarning").css({
            color: "red",
            paddingLeft: "25px"
        });
        $("#emailWarning").text("✗ Некорректный адрес");
    }
}

function showPasswordWarning(inputElem) {
    if ((inputElem.value).length > 5) {
        $("#passwordWarning").css({
            color: "green",
            paddingLeft: ""
        });
        $("#passwordWarning").text("✓");
    } else {
        $("#passwordWarning").css({
            color: "red",
            paddingLeft: "100px"
        });
        $("#passwordWarning").text("✗ Минимум 6 символов");
    }
}

function showPasswordWarningDuplicate(inputElem) {
    var password = $("#signupPassword").val();

    if (password == inputElem.value) {
        $("#passwordWarningDuplicate").css({
            color: "green",
            paddingLeft: ""
        });
        $("#passwordWarningDuplicate").text("✓");
    } else {
        $("#passwordWarningDuplicate").css({
            color: "red",
            paddingLeft: "25px"
        });
        $("#passwordWarningDuplicate").text("✗ Пароли не совпадают");
    }
}

function showNamesWarning(inputElem, spanElement) {
    if (/[\d\s]/i.test(inputElem.value)) {
        $(spanElement).css("color", "red");
        $(spanElement).text("✗ Не может содержать цифр и пробелов");
    } else {
        $(spanElement).css("color", "green");
        $(spanElement).text("✓");
    }
}
/*-=------------------------------------------------- AccountPage --------------------------------------------------=-*/
$(document).ready(function () {
    $('#openPublicMessageToAccountBox').click(function () {
        $('#publicMessageAccountBox').toggle(500, "linear");
    });
});

$(document).ready(function () {
    $('#closePublicMessageBoxToAccount').click(function () {
        $('#publicMessageAccountBox').css("display", "none");
    });
});

$(document).ready(function () {
    $('.groupDescriptionAccountPage').each(function () {

        var text = $(this).text();

        if (text.length > 50) {
            text = text.substring(0, 75);
            $(this).text(text);
            $(this).append('...');
        }
    })

});

/* Pagination for public messages on account page */
$(document).ready(function () {
    function loadPublicMessageWithPagination(elem) {
        if (elem.text() == '1') {
            paginationAccountPage(elem);
        } else {
            paginationAccountPage(elem);
        }
    }

    function paginationAccountPage(elem) {
        $('#accountPublicMessageBox').fadeOut(200, function () {
            $('#accountPublicMessageBox').empty();

            $.ajax({
                url: document.location.origin + '/account/loadPublicMessages?page=' + elem.text() + '&accountId=' + $('#accountIdForPagination').text(),
                type: 'GET',
                dataType: 'json',

                success: function (data) {
                    $.each(data, function (index, item) {
                        $('#accountPublicMessageBox').append('' +
                            '<div class="account-public-msg-div row">' +
                            '<div class="col-md-2" style="padding-top: 22.5%;">' +
                            '<p class="account-label-for-public-messages">' +
                            'СООБЩЕНИЕ' +
                            '</p>' +
                            '</div>' +
                            '<div class="col-md-10">' +
                            '<p>Пользователь ' +
                            '<a href="' + document.location.origin + '/account/user?id=' + item.fromAcc.id + '">' +
                            item.fromAcc.fName + ' ' + item.fromAcc.lName + '</a>' +
                            ' пишет:' +
                            '</p>' +
                            '<p>' + item.description + '</p>' +
                            '</div></div>');
                        if ($('#ownerIdForPagination').text() == $('#accountIdForPagination').text()) {
                            $('#accountPublicMessageBox').append('' +
                                '<div class="account-menu-for-public-message-div">' +
                                '<hr/>' +
                                '<a style="color: #cf524b;" href=' + document.location.origin + '/account/messages/public/remove?messageId=' + item.id + '>' +
                                'Удалить сообщение' +
                                '</a>' +
                                '<a href="#" style="padding-left: 9%; text-decoration: line-through;">Ответить</a>' +
                                '</div>');
                        }
                    });
                }
            });
        }).fadeIn(200);
    }

    $('.paginationButton').click(function () {
        var page = $(this);
        loadPublicMessageWithPagination(page);
    });

    $(window).load(function () {
        if ($('.paginationButtonStart')[0]) {
            loadPublicMessageWithPagination($('.paginationButtonStart'));
        }
    });
});

/*-=------------------------------------------------- RequestsPage -------------------------------------------------=-*/
function confirmRejectFriendRequest() {
    return confirm("Отклонив предложение о дружбе, этот " +
        "пользователь больше не сможет отправлять вам запросы. " +
        "Отклонить дружбу?");
}

/*-=--------------------------------------------------- EditPage ---------------------------------------------------=-*/
function addPhone() {
    var divElem = document.getElementById('phones');

    var newDiv = document.createElement('div');
    divElem.appendChild(newDiv);

    var newIdField = document.createElement('input');
    newIdField.setAttribute("type", "hidden");
    newIdField.setAttribute("name", "phone");
    newDiv.appendChild(newIdField);

    var newLable1 = document.createElement('label');
    newLable1.className += 'acc-descr-for-edit-filed';
    newLable1.innerHTML = 'Описание';
    newDiv.appendChild(newLable1);

    var newInput = document.createElement('input');
    newInput.className += 'form-control';
    newInput.setAttribute("name", "phone")
    newDiv.appendChild(newInput);

    var newLable = document.createElement('label');
    newLable.className += 'acc-descr-for-edit-filed';
    newLable.innerHTML = 'Номер';
    newDiv.appendChild(newLable);

    var newInput = document.createElement('input');
    newInput.className += 'form-control';
    newInput.setAttribute("name", "phone")
    newDiv.appendChild(newInput);

    var newRemoveButton = document.createElement('p');
    newRemoveButton.setAttribute("onclick", "removePhone(this)");
    newRemoveButton.innerHTML = "удалить телефон";
    newDiv.appendChild(newRemoveButton);
}

function removePhone(elem) {
    var parent = elem.parentElement;
    var idInput = parent.firstElementChild;
    var oldValue = idInput.getAttribute("value");
    idInput.setAttribute("value", -oldValue);
    parent.style.display = "none";
}

/*jQuery(function($){
 $("#phone_number").mask("+7 (999) 999-9999",{placeholder:"_"});
 });*/

$(document).ready(function () {
    var max_fields = 5;
    var wrapper = $("#phones");
    var add_button = $("#add_phone");

    var x = 1;
    $(add_button).click(function (e) {
        e.preventDefault();
        if (x < max_fields) {
            x++;
            $(wrapper).append('' +
                '<div style="padding-bottom: 5px;">' +
                '   <input type="hidden" name="phone" value="0"/>' +
                '   <input class="edit-phone-description form-control" type="text" name="phone" placeholder="Описание"/>' +
                '   <input class="edit-phone-number form-control" type="text" name="phone" placeholder="Номер"/>' +
                '   <button id="remove_phone" type="button" class="edit-delete-phone-btn btn btn-danger">&ndash;</button>' +
                '</div>');
        }
    });

    $(wrapper).on("click", "#remove_phone", function (e) {
        e.preventDefault();
        $(this).parent('div').css("display", "none");
        var idInput = $(this).parent('div').children(":first");
        $(idInput).attr("value", -$(idInput).attr("value"));
        x--;
    })
});

/*-=--------------------------------------------------- GroupPage ---------------------------------------------------=-*/
$(document).ready(function () {
    $('#createGroupBtn').click(function () {
        $('#groupList').css("display", "none");
        $('#createGroupDiv').css("display", "");
    });
});

$(document).ready(function () {
    $('#cancelCreateGroup').click(function () {
        $('#groupList').css("display", "");
        $('#createGroupDiv').css("display", "none");
    })
});

$(document).ready(function () {
    $('#searchResultForGroups').hide();
    $('#searchGroupInput').keyup(function () {
        if ($('#searchGroupInput').val().length > 1) {
            $('#searchResultForGroups').empty();
            $('#searchResultForGroups').show();
            $.ajax({
                url: document.location.origin + '/account/group/search?s=' + $('#searchGroupInput').val(),
                type: 'GET',
                dataType: 'json',

                success: function (data) {
                    /*var json_x = $.parseJSON(data);*/
                    $.each(data, function (index, item) {
                        $('#searchResultForGroups').append('' +
                            '<hr/>' +
                            '<a href="' + document.location.origin + '/account/group?id=' + item.id + '">' + item.name + '</a>')
                    });
                }
            });
        } else {
            $('#searchResultForGroups').hide();
        }
    });
});

/*-=----------------------------------------------------- Search ---------------------------------------------------=-*/
/*$(document).ready(function () {
    $('#searchResult').hide();
    $('#searchField').keyup(function () {
        if ($('#searchField').val().length != 0) {
            $('#searchResult').show();
            $.ajax({
                url: 'http://localhost:8080/account/search?s=' + $('#searchField').val(),
                type: 'GET',
                mimeType: "text/html; charset=UTF-8",

                success: function (data) {
                    $('#searchResult').html(data);
                }
            });
        } else {
            $('#searchResult').hide();
        }
    });
});*/

$(document).ready(function () {
    $('#searchResult').hide();
    $('#searchField').keyup(function () {
        if ($('#searchField').val().length > 1) {
            $('#searchResult').empty();
            $('#searchResult').show();
            $.ajax({
                url: document.location.origin + '/account/search?s=' + $('#searchField').val(),
                type: 'GET',
                dataType: 'json',

                success: function (data) {
                    /*var json_x = $.parseJSON(data);*/
                    $.each(data, function (index, item) {
                        $('#searchResult').append('' +
                            '<hr class="header-menu-search-result-separate"/>' +
                            '<a class="header-menu-search-result-a" href="' + document.location.origin + '/account/user?id=' +
                            item.id + '">' +
                            item.fName + ' ' + item.lName +
                            '</a>')
                    });
                }
            });
        } else {
            $('#searchResult').hide();
        }
    });
});

/*-=--------------------------------------------------- MsgsPage ---------------------------------------------------=-*/

$(document).ready(function () {
    /*$('#messageDiv').scrollTop($('#messageDiv').height())*/
    $('#messageDiv').scrollTop(99999);
});

$(document).ready(function () {
    function loadChat() {
        $.ajax({
            // get from session and from url, send it to controller...
        });
    }

    /*loadChat();                     // This will run on page load
     setInterval(function() {
     loadChat()                  // this will run after every 50 seconds
     }, 50000);*/
});

/*function down() {
 var block = document.getElementById("msgbox");
 block.scrollTop = 9999 /!*block.scrollHeight*!/;
 }*/

/*-=------------------------------------------------  O T H E R S --------------------------------------------------=-*/
/*    .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.    .-.
 `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'*/
/*-=-----------------------------------------------  Support methods  ----------------------------------------------=-*/
function checkEmail(email) {
    var array = email.match(/@/g);
    if (array == null || array.length != 1) {
        return false;
    } else {
        return true;
    }
}
