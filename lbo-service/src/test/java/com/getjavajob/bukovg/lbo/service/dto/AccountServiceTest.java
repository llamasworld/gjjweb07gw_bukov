package com.getjavajob.bukovg.lbo.service.dto;

import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.common.model.Phone;
import com.getjavajob.bukovg.lbo.common.model.Request;
import com.getjavajob.bukovg.lbo.dao.SqlAccountDao;
import com.getjavajob.bukovg.lbo.dao.SqlRequestDao;
import com.getjavajob.bukovg.lbo.service.dto.AccountService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @InjectMocks
    private AccountService accountService;
    @Mock
    private SqlAccountDao accountDao;
    @Mock
    private SqlRequestDao requestDao;

    private Account account;
    private List<Account> accounts;
    private List<Request> requests;
    private List<Phone> phones;

    @Before
    public void setUp() {
        Request request = new Request();
        request.setId(1);
        request.setFromAcc(account);

        List<Request> requests = new ArrayList<>();
        requests.add(request);
        this.requests = requests;

        Phone phone = new Phone();
        phone.setId(1);
        phone.setNumber("111-222-333");

        List<Phone> phones = new ArrayList<>();
        phones.add(phone);
        this.phones = phones;

        Account account = new Account();
        account.setId(1);
        account.setfName("TestFName");
        account.setlName("TestLName");
        account.setEmail("mail@mail.ru");
        account.setPassword("112233");
        account.setPhones(phones);
        this.account = account;

        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        this.accounts = accounts;
    }

    @Test
    public void addNewAccount() {
        accountService.addNewAccount(account);
        Mockito.verify(accountDao, Mockito.times(1)).create(any(Account.class));
    }

    @Test
    public void getAccountById() {
        Account halfOfAccount = new Account();
        halfOfAccount.setId(1);
        halfOfAccount.setfName("TestFName");
        halfOfAccount.setlName("TestLName");
        halfOfAccount.setEmail("mail@mail.ru");
        halfOfAccount.setPassword("112233");
        halfOfAccount.setPhones(phones);

        Mockito.when(accountDao.read(1)).thenReturn(halfOfAccount);

        Account actual = accountService.getAccountById(1);

        Mockito.verify(accountDao, Mockito.times(1)).read(anyInt());
        assertEquals(account, actual);
    }

    @Test
    public void updateAccount() {
        accountService.updateAccount(account);
        Mockito.verify(accountDao, Mockito.times(1)).update(account);
    }

    @Test
    public void deleteAccount() {
        accountService.deleteAccount(1);
        Mockito.verify(accountDao, Mockito.times(1)).delete(1);
    }

    @Test
    public void getAllAccounts() {
        Mockito.when(accountDao.getAll()).thenReturn(accounts);
        List<Account> actual = null;
        actual = accountService.getAllAccounts();
        assertNotNull(actual);
    }

    @Test
    public void getAccountByEmailPassword() {
        Account halfOfAccount = new Account();
        halfOfAccount.setId(1);
        halfOfAccount.setfName("TestFName");
        halfOfAccount.setlName("TestLName");
        halfOfAccount.setEmail("mail@mail.ru");
        halfOfAccount.setPassword("112233");

        Mockito.when(accountDao.readAccountByEmailPassword(anyString(), anyString())).thenReturn(halfOfAccount);

        Account actual = accountService.getAccountByEmailPassword("mail@mail.ru", "112233");
        assertNotNull(actual);
    }

    @Test
    public void sendFriendRequest() {
        accountService.sendFriendRequest(new Request());
        Mockito.verify(requestDao, Mockito.times(1)).create(anyObject());
    }

    @Test
    public void getWaitingRequestsForAnAccount() {
        Mockito.when(requestDao.getWaitingRequestsForAnAccount(anyInt())).thenReturn(requests);
        List<Request> actual = accountService.getWaitingRequestsForAnAccount(1);
        assertNotNull(actual);
    }
}
