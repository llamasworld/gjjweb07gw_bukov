package com.getjavajob.bukovg.lbo.service.dto;

import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.common.model.Group;
import com.getjavajob.bukovg.lbo.common.model.Message;
import com.getjavajob.bukovg.lbo.dao.SqlAccountDao;
import com.getjavajob.bukovg.lbo.dao.SqlGroupDao;
import com.getjavajob.bukovg.lbo.dao.SqlMessageDao;
import com.getjavajob.bukovg.lbo.dao.SqlRequestDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Gleb Bukov on 18.06.16.
 */

@Transactional
public class GroupService {

    private SqlAccountDao accountDao;
    private SqlGroupDao groupDao;
    private SqlMessageDao messageDao;

    public GroupService(SqlAccountDao accDao, SqlMessageDao messageDao, SqlGroupDao groupDao) {
        this.accountDao = accDao;
        this.messageDao = messageDao;
        this.groupDao = groupDao;
    }

    public Group getGroupById(int id) {
        Group group = groupDao.read(id);
        group.getMembers().size(); // Collection init
        return group;
    }

    public void createGroup(Group group) {
        groupDao.create(group);
    }

    public void updateGroup(Group group) {
        groupDao.update(group);
    }

    public void leaveGroup(int groupId, int accountId) {
        Account account = accountDao.read(accountId);
        Group group = groupDao.read(groupId);

        account.getGroups().remove(group);
        group.getMembers().remove(account);

        accountDao.update(account);
        groupDao.update(group);
    }

    public List<Message> getPublicMessagesForGroup(int groupId) {
        return messageDao.getPublicMessagesForGroup(groupId);
    }

    public void sendMessageToGroup(Message message) {
        messageDao.create(message);
    }

    public List<Group> findGroupByName(String searchText) {
        return groupDao.findGroupByName(searchText);
    }

    public void removePublicMessagesFromGroup(int messageId) {
        messageDao.delete(messageId);
    }
}
