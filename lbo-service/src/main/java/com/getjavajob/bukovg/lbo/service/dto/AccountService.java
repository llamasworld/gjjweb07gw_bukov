package com.getjavajob.bukovg.lbo.service.dto;

import com.getjavajob.bukovg.lbo.common.model.Group;
import com.getjavajob.bukovg.lbo.common.model.Message;
import com.getjavajob.bukovg.lbo.common.model.Request;
import com.getjavajob.bukovg.lbo.dao.SqlAccountDao;
import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.dao.SqlMessageDao;
import com.getjavajob.bukovg.lbo.dao.SqlRequestDao;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by Gleb Bukov on 18.06.16.
 */

@Transactional
public class AccountService {

    private SqlAccountDao accountDao;
    private SqlRequestDao requestDao;
    private SqlMessageDao messageDao;

    public AccountService(SqlAccountDao accDao, SqlRequestDao requestDao, SqlMessageDao messageDao) {
        this.accountDao = accDao;
        this.requestDao = requestDao;
        this.messageDao = messageDao;
    }

    /*
    - - - - - - - - - - - - - - - - - - - Basic's CRUD methods for models - - - - - - - - - - - - - - - - - - - - - - -
     */
    public void addNewAccount(Account account) {
        account.setPassword(hash256(account.getPassword()));
        accountDao.create(account);
    }

    public Account getAccountById(int id) {
        Account account = accountDao.read(id);
        account.getFriends().size(); // Collection init
        account.getGroups().size(); // Collection init
        for (Group group : account.getGroups()) {
            group.getMembers().size();
        }
        return account;
    }

    public void updateAccount(Account account) {
        accountDao.update(account);
    }

    public void deleteAccount(int id) {
        accountDao.delete(id);
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - Additional 'CRUD' methods - - - - - - - - - - - - - - - - - - - - - - - -
     */
    public List<Account> getAllAccounts() {
        return accountDao.getAll();
    }

    public Account getAccountByEmailPassword(String email, String password) {
        Account account = accountDao.readAccountByEmailPassword(email, hash256(password));
        account.getFriends().size(); // Collection init
        return account;
    }

    public List<Account> findAccountByWord(String word) {
        return accountDao.findAccountByWord(word);
    }

    //———————————————————————————————————————— Friend List + friendship ————————————————————————————————————————————

    public List<Account> getAllFriends(int id) {
        Account account = accountDao.read(id);
        account.getFriends().size(); // Collection init
        return account.getFriends();
    }

    public void removeFriendship(int accountIdOne, int accountIdTwo) {

        Account accountOne = accountDao.read(accountIdOne);
        Account accountTwo = accountDao.read(accountIdTwo);

        accountOne.getFriends().remove(accountTwo);
        accountTwo.getFriends().remove(accountOne);

        accountDao.update(accountOne);
        accountDao.update(accountTwo);
    }

    public Request findFriendRequestByTwoAccountIds(int accountIdOne, int accountIdTwo) {
        return requestDao.findFriendRequestByTwoAccountIds(accountIdOne, accountIdTwo);
    }

    //———————————————————————————————————————————————— Request —————————————————————————————————————————————————————

    public void sendFriendRequest(Request request) {
        requestDao.create(request);
    }

    public List<Request> getWaitingRequestsForAnAccount(int id) {
        return requestDao.getWaitingRequestsForAnAccount(id);
    }

    /**
     * Change 'request' status to RequestStatus.ACCEPTED and
     * add appropriate row to the Friendship table.
     *
     * @param requestId
     */
    public void acceptFriendRequest(int requestId) {
        Request request = requestDao.read(requestId);
        request.setStatus(Request.RequestStatus.ACCEPTED);
        requestDao.update(request);

        Account accountOne = accountDao.read(request.getFromAcc().getId());
        Account accountTwo = accountDao.read(request.getToAcc().getId());

        accountOne.getFriends().add(accountTwo);
        accountTwo.getFriends().add(accountOne);

        accountDao.update(accountOne);
        accountDao.update(accountTwo);
    }

    public void rejectFriendRequest(int requestId) {
        Request request = requestDao.read(requestId);
        request.setStatus(Request.RequestStatus.REJECTED);
        requestDao.update(request);
    }

    //———————————————————————————————————————————————— Message —————————————————————————————————————————————————————

    public List<Message> getMessagesBetweenTwoAccounts(int receiverId, int senderId) {
        return messageDao.getMessagesBetweenTwoAccounts(receiverId, senderId);
    }

    public void sendMessageToAccount(Message message) {
        messageDao.create(message);
    }

    public List<Message> getPublicMessagesForAccount(int accountId) {
        return messageDao.getPublicMessagesForAccount(accountId);
    }

    public Long getTotalPagesForPublicMessagesOfAccount(int accountId) {
        return messageDao.getTotalPagesForPublicMessagesOfAccount(accountId);
    }

    public List<Message> getPublicMessagesForAccountPagination(int accountId, int pageNumber) {
        return messageDao.getPublicMessageForAccountPagination(accountId, pageNumber);
    }

    public void sendPublicMessagesToAccount(Message message) {
        messageDao.create(message);
    }

    public void removePublicMessagesFromAccountsWall(int messageId) {
        messageDao.delete(messageId);
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - - -  Support methods   - - - - - - - - - - - - - - - - - - - - - - - - - -
     */
    private String hash256(String normalText) {
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hash = md.digest(normalText.getBytes("UTF-8"));

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return hexString.toString();
    }
}
