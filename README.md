![alt text](screenshots/0_intro.png "overview")  

# *a*Dating - prototype of social network

Hello! I'm glad to introduce you my final project at *gjj07 course*. It is a small light version of popular online social networking services such as facebook.com, vk.com and ok.ru. I tried to use all the technologies I've learned during this course. More information as well as a full list of technologies is available in the history of commits.

** Functionality: **The main functional that I implemented is listed below:

+ registration
+ ajax loading of accounts and groups
+ authentication (with remember me flag)
+ ajax search with pagination
+ display profile
+ edit profile
+ upload and download avatar
+ display group
+ edit group
+ add and remove friends
+ add and remove groups
+ chatting with friends
+ public messages on account page

** Tools: **
I used different technologies to implement the project. More informatiom is avaliable in the history of commits:

JDK 7/8, Spring 4, JPA 2 / Hibernate 4, XStream, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 7, MySQL, IntelliJIDEA 14.

** Screenshots **

Welcome page:
![alt text](screenshots/1_welcomePage.jpg "Welcome Page")

Account page:
![alt text](screenshots/2_accountPage.jpg "Account Page")

Private chatting:
![alt text](screenshots/3_privateMessagePage.jpg "Private chatting")

Group overview:
![alt text](screenshots/4_groupOverviewPage.jpg "Group Overview")

Group page:
![alt text](screenshots/5_groupPage.jpg "Group page")

--  
**Буков Глеб \ Bukov Gleb**  
Тренинг getJavaJob,  
[http://www.getjavajob.com](http://www.getjavajob.com)