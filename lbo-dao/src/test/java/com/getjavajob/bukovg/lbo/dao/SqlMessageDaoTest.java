package com.getjavajob.bukovg.lbo.dao;

import com.getjavajob.bukovg.lbo.common.model.Message;
import org.junit.*;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

public class SqlMessageDaoTest extends BasicDaoTest {

    @Test
    public void create() {
        Message expected = new Message();
        expected.setFromAcc(accountDao.read(2));
        expected.setToAcc(accountDao.read(1));
        expected.setType(Message.MessageType.PRIVATE_MSG);
        expected.setDescription("Test text...");
        messageDao.create(expected);
        Message actual = messageDao.read(18);
        assertEquals(expected, actual);
    }

    @Test
    public void read() {
        Message actual = messageDao.read(2);
        assertNotNull(actual);
        actual = messageDao.read(18);
        assertNull(actual);
    }

    @Test
    public void update() {
        Message expected = messageDao.read(1);
        expected.setDescription("Bomba-leila");
        messageDao.update(expected);
        Message actual = messageDao.read(1);
        assertEquals(expected, actual);
    }

    @Test
    public void getAll() {
        List<Message> actual = messageDao.getAll();
        assertTrue(actual.size() == 17);
    }

    @Test
    public void getMessagesBetweenTwoAccounts() {
        List<Message> actual = messageDao.getMessagesBetweenTwoAccounts(1, 2);
        assertTrue(actual.size() == 4);
        actual = messageDao.getMessagesBetweenTwoAccounts(3, 2);
        assertTrue(actual.size() == 2);
    }

    @Test
    public void getPublicMessagesForAccount() {
        List<Message> actual = messageDao.getPublicMessagesForAccount(1);
        assertTrue(actual.size() == 3);
    }

    @Test
    public void getPublicMessagesForAccountPagination() {
        List<Message> actual = messageDao.getPublicMessageForAccountPagination(2, 2);
        assertTrue(actual.size() == 1);
        actual = messageDao.getPublicMessageForAccountPagination(2, 1);
        assertTrue(actual.size() == 5);
    }

    @Test
    public void getPublicMessagesForGroup() {
        List<Message> actual = messageDao.getPublicMessagesForGroup(1);
        assertNotNull(actual);
        assertTrue(actual.size() == 2);
    }

    @Test
    public void getTotalPagesForPublicMessagesOfAccount() {
        assertEquals(new Long(2), messageDao.getTotalPagesForPublicMessagesOfAccount(2));
    }
}
