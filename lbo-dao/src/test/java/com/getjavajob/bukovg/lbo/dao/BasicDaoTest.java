package com.getjavajob.bukovg.lbo.dao;

import org.h2.tools.RunScript;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Gleb Bukov on 02.08.16.
 */

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-overrides.xml"})
public abstract class BasicDaoTest {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    protected DataSource dataSource;
    @Autowired
    protected SqlAccountDao accountDao;
    @Autowired
    protected SqlMessageDao messageDao;
    @Autowired
    protected SqlRequestDao requestDao;
    @Autowired
    protected SqlGroupDao groupDao;

    private Connection connection;

    @Before
    public void setUp() throws SQLException, IOException {
        connection = dataSource.getConnection();

        // create and fill tables from sql script
        try (InputStreamReader inputStreamReader = new InputStreamReader(SqlAccountDaoTest.class.getClassLoader().getResourceAsStream("createTables.sql"))) {
            RunScript.execute(connection, inputStreamReader);
        }
    }

    @After
    public void tearDown() throws SQLException {
        connection.close();
    }
}
