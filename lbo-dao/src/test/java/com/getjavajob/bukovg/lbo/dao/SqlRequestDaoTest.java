package com.getjavajob.bukovg.lbo.dao;

import com.getjavajob.bukovg.lbo.common.model.Request;
import org.junit.*;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

public class SqlRequestDaoTest extends BasicDaoTest {

    @Test
    public void create() {
        Request expected = new Request();
        expected.setFromAcc(accountDao.read(2));
        expected.setToAcc(accountDao.read(1));
        expected.setType(Request.RequestType.FRIEND_REQ);
        requestDao.create(expected);
        Request actual = requestDao.read(4);
        assertEquals(expected, actual);
    }

    @Test
    public void read() {
        Request actual = requestDao.read(1);
        assertNotNull(actual);
    }

    @Test
    public void update() {
        Request expected = requestDao.read(1);
        expected.setStatus(Request.RequestStatus.ACCEPTED);
        requestDao.update(expected);
        Request actual = requestDao.read(1);
        assertEquals(expected, actual);
    }

    @Test
    public void getAll() {
        List<Request> actual = requestDao.getAll();
        assertTrue(actual.size() == 3);
    }

    @Test
    public void getIncomingAccountsRequests() {
        List<Request> requests = requestDao.getWaitingRequestsForAnAccount(1);
        assertTrue(requests.size() == 2);
    }

    @Test
    public void findFriendRequestByTwoAccountIds() {
        assertNotNull(requestDao.findFriendRequestByTwoAccountIds(1, 3));
    }
}
