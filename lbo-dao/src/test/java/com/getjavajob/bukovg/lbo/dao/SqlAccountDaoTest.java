package com.getjavajob.bukovg.lbo.dao;

import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.common.model.Phone;
import com.getjavajob.bukovg.lbo.common.type.Role;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

public class SqlAccountDaoTest extends BasicDaoTest {

    @Test
    public void create() {
        Account expected = new Account();
        expected.setEmail("test4@test4.ru");
        expected.setPassword("test4");
        expected.setfName("Test4");
        expected.setlName("Testovich4");
        expected.setRole(Role.admin);

        Phone phone1 = new Phone();
        phone1.setOwner(expected);
        phone1.setNumber("111-222");
        phone1.setDescription("home");

        Phone phone2 = new Phone();
        phone2.setOwner(expected);
        phone2.setNumber("333-444");
        phone2.setDescription("work");

        List<Phone> phones = new ArrayList<>();
        phones.add(phone1);
        phones.add(phone2);

        expected.setPhones(phones);

        accountDao.create(expected);

        Account actual = accountDao.read(6);
        assertEquals(expected, actual);
    }

    @Test
    public void read() {
        Account actual = accountDao.read(1);
        assertNotNull(actual);
        assertTrue(actual.getPhones().size() == 3);
        assertTrue(actual.getFriends().size() == 2);
    }

    @Test
    public void update() {
        Account expected = accountDao.read(1);
        expected.setlName("lastName1-1");
        accountDao.update(expected);

        entityManager.clear();

        Account actual = accountDao.read(1);
        assertEquals(expected, actual);
    }

    @Test
    public void getAll() {
        List<Account> actual = accountDao.getAll();
        assertTrue(actual.size() == 5);
    }

    @Test
    public void readAccountByLoginAndPassword() {
        Account actual = accountDao.readAccountByEmailPassword("test3@m.ru", "123");
        assertNotNull(actual);
    }

    @Test
    public void findAccountByWord() {
        List<Account> actual = accountDao.findAccountByWord("na");
        assertTrue(actual.size() == 5);
    }

    @Test
    public void getAllFriends() {
        List<Account> actual = accountDao.read(1).getFriends();
        assertTrue(actual.size() == 2);
    }
}
