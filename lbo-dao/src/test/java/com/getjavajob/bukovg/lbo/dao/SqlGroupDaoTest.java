package com.getjavajob.bukovg.lbo.dao;

import com.getjavajob.bukovg.lbo.common.model.Account;
import com.getjavajob.bukovg.lbo.common.model.Group;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Gleb Bukov on 02.08.16.
 */

public class SqlGroupDaoTest extends BasicDaoTest {

    @Test
    public void create() {
        Account owner = accountDao.read(1);

        Group group = new Group();
        group.setOwner(owner);
        group.setName("GroupTest");
        group.setDescription("Long descripti-o-o-n-n-n");
        group.getMembers().add(owner);

        groupDao.create(group);

        group.getMembers().add(accountDao.read(3));
        group.getMembers().add(accountDao.read(4));
        group.getMembers().add(accountDao.read(5));

        Group actual = groupDao.read(3);

        assertNotNull(actual);
        assertTrue(actual.getMembers().size() == 4);
    }

    @Test
    public void read() {
        Group actual = groupDao.read(2);
        assertNotNull(actual);
        List<Account> members = actual.getMembers();
        assertTrue(members.size() == 3);
    }

    @Test
    public void update() {
        Group actual = groupDao.read(1);
        assertEquals(actual.getDescription(), "Description...1");

        entityManager.clear();

        Group expected = groupDao.read(1);
        expected.setDescription("Description...1...1...1...1..");
        groupDao.update(expected);

        entityManager.clear();

        actual = groupDao.read(1);
        assertEquals(actual, expected);

    }

    @Test
    public void getAll() {
        assertTrue(groupDao.getAll().size() == 2);
    }
}

