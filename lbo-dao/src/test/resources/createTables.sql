DROP TABLE IF EXISTS Accounts;

CREATE TABLE Accounts (
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(255) NOT NULL,
password VARCHAR(64) NOT NULL,
first_name VARCHAR(20) NOT NULL,
last_name VARCHAR(20) NOT NULL,
pron_name VARCHAR(20) NULL,
skype VARCHAR(255) NULL,
icq INT NULL,
enrollment_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
role VARCHAR(20) NOT NULL CHECK role IN ('user', 'admin'),
photo BLOB NULL,
content_type VARCHAR(20) NULL,
PRIMARY KEY (`id`));

DROP TABLE IF EXISTS Phones;

CREATE TABLE Phones (
id INT NOT NULL AUTO_INCREMENT,
phone_owner INT NOT NULL,
number VARCHAR(30) NOT NULL,
description VARCHAR(15) NULL,
PRIMARY KEY (`id`),
CONSTRAINT phone_owner
FOREIGN KEY (phone_owner)
REFERENCES Accounts (id));

DROP TABLE IF EXISTS Friendship;

CREATE TABLE Friendship (
id_acc_one INT NOT NULL,
id_acc_two INT NOT NULL,
PRIMARY KEY (`id_acc_one`, `id_acc_two`));

DROP TABLE IF EXISTS Groups;

CREATE TABLE Groups (
id INT NOT NULL AUTO_INCREMENT,
owner_id INT NOT NULL,
name VARCHAR(100) NOT NULL,
enrollment_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
description VARCHAR(1000) NULL,
PRIMARY KEY (id),
photo BLOB NULL,
content_type VARCHAR(20) NULL,
CONSTRAINT ownerid
FOREIGN KEY (owner_id)
REFERENCES Accounts (id));

DROP TABLE IF EXISTS Messages;

CREATE TABLE Messages (
id INT NOT NULL AUTO_INCREMENT,
date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
from_id INT NOT NULL,
to_acc_id INT NULL,
to_group_id INT NULL,
type VARCHAR(20) NOT NULL CHECK type IN ('GROUP_MSG', 'PRIVATE_MSG', 'PUBLIC_MSG'),
description VARCHAR(1000) NULL,
picture BLOB NULL,
PRIMARY KEY (id),
CONSTRAINT fromidmes
FOREIGN KEY (from_id)
REFERENCES Accounts (id),
CONSTRAINT toaccmes
FOREIGN KEY (to_acc_id)
REFERENCES Accounts (id),
CONSTRAINT togroupmes
FOREIGN KEY (to_group_id)
REFERENCES Groups (id));

DROP TABLE IF EXISTS Requests;

CREATE TABLE Requests (
id INT NOT NULL AUTO_INCREMENT,
date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
from_id INT NOT NULL,
to_acc_id INT NULL,
/*to_group_id INT NULL,*/
type VARCHAR(20) NOT NULL CHECK type IN ('FRIEND_REQ', 'GROUP_REQ'),
status VARCHAR(20) NOT NULL CHECK status IN ('ACCEPTED', 'REJECTED', 'WAITING'),
PRIMARY KEY (`id`),
CONSTRAINT fromid
FOREIGN KEY (from_id)
REFERENCES Accounts (id),
CONSTRAINT toaccid
FOREIGN KEY (to_acc_id)
REFERENCES Accounts (id)/*,
CONSTRAINT togroupid
FOREIGN KEY (to_group_id)
REFERENCES Groups (id)*/);

DROP TABLE IF EXISTS Groups_Accounts;

CREATE TABLE Groups_Accounts (
id_group INT NOT NULL,
id_account INT NOT NULL,
PRIMARY KEY (`id_group`, `id_account`),
CONSTRAINT idgroup
FOREIGN KEY (id_group)
REFERENCES Groups (id),
CONSTRAINT idaccount
FOREIGN KEY (id_account)
REFERENCES Accounts (id));

/* ------------------------------------------------ Accounts -------------------------------------------------------- */

INSERT INTO Accounts (id, email, password, first_name, last_name, role)
VALUES (1, 'test1@m.ru', '123', 'name1', 'lastName1', 'user');
INSERT INTO Accounts (id, email, password, first_name, last_name, pron_name, role)
VALUES (2, 'test2@m.ru', '123', 'name2', 'lastName2', 'pronName2' ,'user');
INSERT INTO Accounts (id, email, password, first_name, last_name, role)
VALUES (3, 'test3@m.ru', '123', 'name3', 'lastName3', 'user');
INSERT INTO Accounts (id, email, password, first_name, last_name, role)
VALUES (4, 'test4@m.ru', '123', 'name4', 'lastName4', 'user');
INSERT INTO Accounts (id, email, password, first_name, last_name, role)
VALUES (5, 'test5@m.ru', '123', 'name5', 'lastName5', 'user');

/* ------------------------------------------------- Phones --------------------------------------------------------- */

INSERT INTO Phones (id, phone_owner, number)
VALUES (1, 1, '1111');
INSERT INTO Phones (id, phone_owner, number)
VALUES (2, 1, '2222');
INSERT INTO Phones (id, phone_owner, number)
VALUES (3, 1, '3333');
INSERT INTO Phones (id, phone_owner, number)
VALUES (4, 2, '4444');

/* ---------------------------------------------- Friendship -------------------------------------------------------- */

INSERT INTO Friendship (id_acc_one, id_acc_two)
VALUES (1, 2);
INSERT INTO Friendship (id_acc_one, id_acc_two)
VALUES (2, 1);
INSERT INTO Friendship (id_acc_one, id_acc_two)
VALUES (1, 3);
INSERT INTO Friendship (id_acc_one, id_acc_two)
VALUES (3, 1);
INSERT INTO Friendship (id_acc_one, id_acc_two)
VALUES (2, 3);
INSERT INTO Friendship (id_acc_one, id_acc_two)
VALUES (3, 2);

/* ------------------------------------------- Private Messages ----------------------------------------------------- */

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (1, 1, 2, 'PRIVATE_MSG', 'The first message');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (2, 1, 2, 'PRIVATE_MSG', 'The second message');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (3, 2, 1, 'PRIVATE_MSG', 'The third message');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (4, 3, 2, 'PRIVATE_MSG', 'The fourth message');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (5, 3, 2, 'PRIVATE_MSG', 'The fifth message');

/* ------------------------------------------- Public Messages ----------------------------------------------------- */

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (6, 2, 1, 'PUBLIC_MSG', 'The first public message');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (7, 3, 1, 'PUBLIC_MSG', 'The second public message');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (8, 4, 1, 'PUBLIC_MSG', 'The third public message');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (12, 4, 2, 'PUBLIC_MSG', 'Message for testing pagination');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (13, 4, 2, 'PUBLIC_MSG', 'Message for testing pagination');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (14, 4, 2, 'PUBLIC_MSG', 'Message for testing pagination');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (15, 4, 2, 'PUBLIC_MSG', 'Message for testing pagination');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (16, 4, 2, 'PUBLIC_MSG', 'Message for testing pagination');

INSERT INTO Messages (id, from_id, to_acc_id, type, description)
VALUES (17, 4, 2, 'PUBLIC_MSG', 'Message for testing pagination');

/* -------------------------------------------- Request (friend) ---------------------------------------------------- */

INSERT INTO Requests (id, from_id, to_acc_id, type, status)
VALUES (1, 2, 1, 'FRIEND_REQ', 'WAITING');

INSERT INTO Requests (id, from_id, to_acc_id, type, status)
VALUES (2, 3, 1, 'FRIEND_REQ', 'WAITING');

INSERT INTO Requests (id, from_id, to_acc_id, type, status)
VALUES (3, 2, 3, 'FRIEND_REQ', 'WAITING');

/* ------------------------------------------------- Groups --------------------------------------------------------- */

INSERT INTO Groups (id, owner_id, name, description)
VALUES (1, 1, 'TestGroup1', 'Description...1');

INSERT INTO Groups (id, owner_id, name, description)
VALUES (2, 2, 'TestGroup2', 'Description...2');

/* --------------------------------------------- Account_Group ------------------------------------------------------ */

INSERT INTO Groups_Accounts (id_group, id_account)
VALUES (1, 3);

INSERT INTO Groups_Accounts (id_group, id_account)
VALUES (2, 3);

INSERT INTO Groups_Accounts (id_group, id_account)
VALUES (2, 4);

INSERT INTO Groups_Accounts (id_group, id_account)
VALUES (2, 5);

/* ------------------------------------------- Group Messages ----------------------------------------------------- */

INSERT INTO Messages (id, from_id, to_group_id, type, description)
VALUES (9, 2, 1, 'PUBLIC_MSG', 'The first public message for group');

INSERT INTO Messages (id, from_id, to_group_id, type, description)
VALUES (10, 3, 1, 'PUBLIC_MSG', 'The second public message for group');

INSERT INTO Messages (id, from_id, to_group_id, type, description)
VALUES (11, 4, 2, 'PUBLIC_MSG', 'The third public message for group');