package com.getjavajob.bukovg.lbo.dao;

import com.getjavajob.bukovg.lbo.common.model.Group;
import com.getjavajob.bukovg.lbo.common.model.Group_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Gleb Bukov on 15.06.16.
 */
public class SqlGroupDao extends GenericDao<Group> {

    public SqlGroupDao() {
        super(Group.class);
    }

    public List<Group> findGroupByName(String word) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> root = query.from(Group.class);
        query.select(root).where(builder.like(root.get(Group_.name), "%" + word + "%"));
        return entityManager.createQuery(query).getResultList();
    }
}
