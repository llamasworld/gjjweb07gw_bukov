package com.getjavajob.bukovg.lbo.dao;

import com.getjavajob.bukovg.lbo.common.model.Request;
import com.getjavajob.bukovg.lbo.common.model.Request_;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

public class SqlRequestDao extends GenericDao<Request> {

    public SqlRequestDao() {
        super(Request.class);
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - Additional 'CRUD' methods JPA  - - - - - - - - - - - - - - - - - - - - - -
    */
    public List<Request> getWaitingRequestsForAnAccount(int accountId) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Request> query = builder.createQuery(Request.class);
        Root<Request> root = query.from(Request.class);
        query.select(root).where(
                builder.and(
                        builder.equal(root.get(Request_.toAcc), accountId),
                        builder.equal(root.get(Request_.status), Request.RequestStatus.WAITING)
                )
        );
        return entityManager.createQuery(query).getResultList();
    }

    public Request findFriendRequestByTwoAccountIds(int accountIdOne, int accountIdTwo) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Request> query = builder.createQuery(Request.class);
        Root<Request> root = query.from(Request.class);
        query.select(root).where(
                builder.or(
                        builder.and(
                                builder.equal(root.get(Request_.fromAcc), accountIdOne),
                                builder.equal(root.get(Request_.toAcc), accountIdTwo)
                        ),
                        builder.and(
                                builder.equal(root.get(Request_.toAcc), accountIdOne),
                                builder.equal(root.get(Request_.fromAcc), accountIdTwo)
                        )
                )
        );
        try {
            return entityManager.createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }
}
