package com.getjavajob.bukovg.lbo.dao;

import com.getjavajob.bukovg.lbo.common.model.Account_;
import com.getjavajob.bukovg.lbo.common.model.Account;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Gleb Bukov on 15.06.16.
 */
public class SqlAccountDao extends GenericDao<Account> {

    public SqlAccountDao() {
        super(Account.class);
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - Additional 'CRUD' methods JPA  - - - - - - - - - - - - - - - - - - - - - -
    */
    public Account readAccountByEmailPassword(String email, String password) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);  // Типизорованный экз query
        Root<Account> root = query.from(Account.class);                     // Создаем query expressions
                                                                            // поиск среди всех хранимых объектов Account
        /*Predicate condition = builder.equal(root.get("email"), email);
        query.select(root).where(condition);*/
        query.select(root).where(
                builder.equal(root.get(Account_.email), email),
                builder.equal(root.get(Account_.password), password));
        return entityManager.createQuery(query).getSingleResult();
    }

    public List<Account> findAccountByWord(String word) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> root = query.from(Account.class);
        query.select(root).where(
                builder.or(
                        builder.like(
                                root.get(Account_.fName), "%" + word + "%"),
                        builder.like(
                                root.get(Account_.lName), "%" + word + "%")
                )
        );
        return entityManager.createQuery(query).getResultList();
    }
}
