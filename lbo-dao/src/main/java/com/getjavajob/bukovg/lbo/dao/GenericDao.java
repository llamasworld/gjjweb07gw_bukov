package com.getjavajob.bukovg.lbo.dao;

import com.getjavajob.bukovg.lbo.common.model.GenericModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Gleb Bukov on 15.06.16.
 */
public abstract class GenericDao<T extends GenericModel> {

    @PersistenceContext
    protected EntityManager entityManager;
    protected Class<T> genericClass;

    public GenericDao(Class<T> genericClass) {
        this.genericClass = genericClass;
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - - Basic CRUD methods / JPA - - - - - - - - - - - - - - - - - - - - - - - -
     */
    public void create(T object) {
        entityManager.persist(object);
    }

    public T read(int id) {
        return entityManager.find(genericClass, id);
    }

    public void update(T object) {
        entityManager.merge(object);
    }

    public void delete(int id) {
        T object = entityManager.find(genericClass, id);
        if (object != null) {
            entityManager.remove(object);
        }
    }

    public List<T> getAll() {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(genericClass);
        Root<T> from = criteriaQuery.from(genericClass);
        CriteriaQuery<T> select = criteriaQuery.select(from);
        List<T> objects = entityManager.createQuery(select).getResultList();

        return objects;
    }
}
