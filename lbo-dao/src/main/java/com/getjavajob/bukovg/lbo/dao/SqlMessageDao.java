package com.getjavajob.bukovg.lbo.dao;

import com.getjavajob.bukovg.lbo.common.model.Message;
import com.getjavajob.bukovg.lbo.common.model.Message_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Gleb Bukov on 15.06.16.
 */

public class SqlMessageDao extends GenericDao<Message> {

    public SqlMessageDao() {
        super(Message.class);
    }

    /*
    - - - - - - - - - - - - - - - - - - - - - Additional 'CRUD' methods JPA  - - - - - - - - - - - - - - - - - - - - - -
    */
    public List<Message> getMessagesBetweenTwoAccounts(int receiverId, int senderId) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> query = builder.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);
        query.select(root).where(
                builder.or(
                        builder.and(
                                builder.equal(root.get(Message_.toAcc), receiverId),
                                builder.equal(root.get(Message_.fromAcc), senderId)),
                        builder.and(
                                builder.equal(root.get(Message_.fromAcc), receiverId),
                                builder.equal(root.get(Message_.toAcc), senderId))
                )
        );
        return entityManager.createQuery(query).getResultList();
    }

    public List<Message> getPublicMessagesForAccount(int accountId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> query = builder.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);
        query.select(root).where(
                builder.and(
                        builder.equal(root.get(Message_.toAcc), accountId),
                        builder.equal(root.get(Message_.type), Message.MessageType.PUBLIC_MSG))
        );
        query.orderBy(builder.desc(root.get(Message_.id)));
        return entityManager.createQuery(query).getResultList();
    }

    public List<Message> getPublicMessageForAccountPagination(int accountId, int pageNumber) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> query = builder.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);
        query.select(root).where(
                builder.and(
                        builder.equal(root.get(Message_.toAcc), accountId),
                        builder.equal(root.get(Message_.type), Message.MessageType.PUBLIC_MSG))
        );
        query.orderBy(builder.desc(root.get(Message_.id)));

        int pageSize = 5;
        TypedQuery typedQuery = entityManager.createQuery(query);
        typedQuery.setFirstResult((pageNumber - 1) * pageSize);
        typedQuery.setMaxResults(pageSize);
        List<Message> result = typedQuery.getResultList();
        return result;
    }

    /**
     * Calculates number of pages for public messages on the account's wall.
     * Minimum will be always 1.
     *
     * @param accountId
     * @return numbers of pages with public messages
     */
    public Long getTotalPagesForPublicMessagesOfAccount(int accountId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Message> root = query.from(Message.class);
        query.select(builder.count(root)).where(
                builder.and(
                        builder.equal(root.get(Message_.toAcc), accountId),
                        builder.equal(root.get(Message_.type), Message.MessageType.PUBLIC_MSG))
        );
        long pageSize = 5;
        long totalRows = entityManager.createQuery(query).getSingleResult();
        long totalPages = totalRows / pageSize;
        if (totalPages == 0) {
            totalPages++;
        } else {
            totalPages = totalRows % pageSize > 0 ? ++totalPages : totalPages;
        }
        return totalPages;
    }

    public List<Message> getPublicMessagesForGroup(int groupId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> query = builder.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);
        query.select(root).where(
                builder.and(
                        builder.equal(root.get(Message_.toGroup), groupId),
                        builder.equal(root.get(Message_.type), Message.MessageType.PUBLIC_MSG))
        );
        query.orderBy(builder.desc(root.get(Message_.id)));
        return entityManager.createQuery(query).getResultList();
    }
}
